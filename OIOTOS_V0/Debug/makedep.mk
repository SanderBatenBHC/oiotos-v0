################################################################################
# Automatically-generated file. Do not edit or delete the file
################################################################################

atmel_start.c

cryptoauthlib\lib\atcacert\atcacert_client.c

cryptoauthlib\lib\atcacert\atcacert_date.c

cryptoauthlib\lib\atcacert\atcacert_def.c

cryptoauthlib\lib\atcacert\atcacert_der.c

cryptoauthlib\lib\atcacert\atcacert_host_hw.c

cryptoauthlib\lib\atcacert\atcacert_host_sw.c

cryptoauthlib\lib\atca_cfgs.c

cryptoauthlib\lib\atca_command.c

cryptoauthlib\lib\atca_device.c

cryptoauthlib\lib\atca_iface.c

cryptoauthlib\lib\basic\atca_basic.c

cryptoauthlib\lib\basic\atca_helpers.c

cryptoauthlib\lib\crypto\atca_crypto_sw_ecdsa.c

cryptoauthlib\lib\crypto\atca_crypto_sw_rand.c

cryptoauthlib\lib\crypto\atca_crypto_sw_sha1.c

cryptoauthlib\lib\crypto\atca_crypto_sw_sha2.c

cryptoauthlib\lib\crypto\hashes\sha1_routines.c

cryptoauthlib\lib\crypto\hashes\sha2_routines.c

cryptoauthlib\lib\hal\atca_hal.c

cryptoauthlib\lib\host\atca_host.c

cryptoauthlib\lib\tls\atcatls.c

cryptoauthlib_main.c

Device_Startup\startup_samg55j19.c

Device_Startup\system_samg55j19.c

driver_init.c

examples\driver_examples.c

hal\src\hal_adc_os.c

hal\src\hal_atomic.c

hal\src\hal_crc_sync.c

hal\src\hal_delay.c

hal\src\hal_gpio.c

hal\src\hal_i2c_m_sync.c

hal\src\hal_init.c

hal\src\hal_io.c

hal\src\hal_sleep.c

hal\src\hal_spi_m_sync.c

hal\src\hal_usart_sync.c

hal\utils\src\utils_assert.c

hal\utils\src\utils_event.c

hal\utils\src\utils_list.c

hal\utils\src\utils_ringbuffer.c

hal\utils\src\utils_syscalls.c

hpl\adc\hpl_adc.c

hpl\core\hpl_core_m4_base.c

hpl\core\hpl_init.c

hpl\crccu\hpl_crccu.c

hpl\mem2mem\hpl_mem2mem.c

hpl\pmc\hpl_pmc.c

hpl\pmc\hpl_sleep.c

hpl\spi\hpl_spi.c

hpl\systick\hpl_systick_ARMv7_base.c

hpl\twi\hpl_twi.c

hpl\usart\hpl_usart.c

main.c

rtos_start.c

Sources\NfcLibrary\NdefLibrary\src\P2P_NDEF.c

Sources\NfcLibrary\NdefLibrary\src\RW_NDEF.c

Sources\NfcLibrary\NdefLibrary\src\RW_NDEF_T1T.c

Sources\NfcLibrary\NdefLibrary\src\RW_NDEF_T2T.c

Sources\NfcLibrary\NdefLibrary\src\RW_NDEF_T3T.c

Sources\NfcLibrary\NdefLibrary\src\RW_NDEF_T4T.c

Sources\NfcLibrary\NdefLibrary\src\T4T_NDEF_emu.c

Sources\NfcLibrary\NxpNci\src\NxpNci.c

Sources\TML\src\tml.c

thirdparty\RTOS\freertos\FreeRTOSV8.2.3\rtos_port.c

thirdparty\RTOS\freertos\FreeRTOSV8.2.3\Source\croutine.c

thirdparty\RTOS\freertos\FreeRTOSV8.2.3\Source\event_groups.c

thirdparty\RTOS\freertos\FreeRTOSV8.2.3\Source\list.c

thirdparty\RTOS\freertos\FreeRTOSV8.2.3\Source\portable\GCC\ARM_CM4F\port.c

thirdparty\RTOS\freertos\FreeRTOSV8.2.3\Source\portable\MemMang\heap_1.c

thirdparty\RTOS\freertos\FreeRTOSV8.2.3\Source\queue.c

thirdparty\RTOS\freertos\FreeRTOSV8.2.3\Source\tasks.c

thirdparty\RTOS\freertos\FreeRTOSV8.2.3\Source\timers.c


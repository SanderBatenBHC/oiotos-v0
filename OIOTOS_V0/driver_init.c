/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */

#include "driver_init.h"
#include <hal_init.h>
#include <hpl_pmc.h>
#include <peripheral_clk_config.h>
#include <utils.h>

#include <hpl_usart_base.h>

#include <hpl_usart_base.h>

/* The channel amount for ADC */
#define ADC_0_CH_AMOUNT 1

/* The buffer size for ADC */
#define ADC_0_CH0_BUF_SIZE 16

/* The maximal channel number of enabled channels */ #define ADC_0_CH_MAX 0

    struct adc_os_descriptor     ADC_0;
struct adc_os_channel_descriptor ADC_0_ch[ADC_0_CH_AMOUNT];
struct crc_sync_descriptor       CRC_0;
struct spi_m_sync_descriptor     SPI_0;

static uint8_t ADC_0_ch0_buf[ADC_0_CH0_BUF_SIZE];

#ifdef ADC_0_CH_MAX
static uint8_t ADC_0_map[ADC_0_CH_MAX + 1];
#endif

struct usart_sync_descriptor USART_0;

struct usart_sync_descriptor USART_1;

struct i2c_m_sync_desc I2C_0;

/**
 * \brief ADC initialization function
 *
 * Enables ADC peripheral, clocks and initializes ADC driver
 */
static void ADC_0_init(void)
{
	_pmc_enable_periph_clock(ID_ADC);
#ifdef ADC_0_CH_MAX
	adc_os_init(&ADC_0, ADC, ADC_0_map, ADC_0_CH_MAX, ADC_0_CH_AMOUNT, &ADC_0_ch[0]);
#endif
	adc_os_register_channel_buffer(&ADC_0, CONF_ADC_0_CHANNEL_0, ADC_0_ch0_buf, ADC_0_CH0_BUF_SIZE);

	gpio_set_pin_function(PA17, GPIO_PIN_FUNCTION_OFF);
}

void delay_driver_init(void)
{
	delay_init(SysTick);
}

/**
 * \brief CRC initialization function
 *
 * Enables CRC peripheral, clocks and initializes CRC driver
 */
void CRC_0_init(void)
{
	_pmc_enable_periph_clock(ID_CRCCU);
	crc_sync_init(&CRC_0, CRCCU);
}

void USART_0_PORT_init(void)
{

	gpio_set_pin_function(PA9, PINMUX_PA9A_FLEXCOM0_RXD);

	gpio_set_pin_function(PA10, PINMUX_PA10A_FLEXCOM0_TXD);
}

void USART_0_CLOCK_init(void)
{
	_pmc_enable_periph_clock(ID_FLEXCOM0);
}

void USART_0_init(void)
{
	USART_0_CLOCK_init();
	USART_0_PORT_init();
	usart_sync_init(&USART_0, FLEXCOM0, _usart_get_usart_sync());
}

void USART_1_PORT_init(void)
{

	gpio_set_pin_function(PB2, PINMUX_PB2A_FLEXCOM1_RXD);

	gpio_set_pin_function(PB3, PINMUX_PB3A_FLEXCOM1_TXD);
}

void USART_1_CLOCK_init(void)
{
	_pmc_enable_periph_clock(ID_FLEXCOM1);
}

void USART_1_init(void)
{
	USART_1_CLOCK_init();
	USART_1_PORT_init();
	usart_sync_init(&USART_1, FLEXCOM1, _usart_get_usart_sync());
}

void SPI_0_PORT_init(void)
{

	gpio_set_pin_function(PA5, PINMUX_PA5A_FLEXCOM2_MISO);

	gpio_set_pin_function(PA6, PINMUX_PA6A_FLEXCOM2_MOSI);

	gpio_set_pin_function(PA15, PINMUX_PA15B_FLEXCOM2_SPCK);
}

void SPI_0_CLOCK_init(void)
{
	_pmc_enable_periph_clock(ID_FLEXCOM2);
}

void SPI_0_init(void)
{
	SPI_0_CLOCK_init();
	spi_m_sync_init(&SPI_0, FLEXCOM2);
	SPI_0_PORT_init();
}

void I2C_0_PORT_init(void)
{

	gpio_set_pin_function(PA4, PINMUX_PA4A_FLEXCOM3_TWCK);

	gpio_set_pin_function(PA3, PINMUX_PA3A_FLEXCOM3_TWD);
}

void I2C_0_CLOCK_init(void)
{
	_pmc_enable_periph_clock(ID_FLEXCOM3);
}

void I2C_0_init(void)
{
	I2C_0_CLOCK_init();
	i2c_m_sync_init(&I2C_0, FLEXCOM3);
	I2C_0_PORT_init();
}

void system_init(void)
{
	init_mcu();

	_pmc_enable_periph_clock(ID_PIOA);

	_pmc_enable_periph_clock(ID_PIOB);

	/* Disable Watchdog*/
	hri_wdt_set_MR_WDDIS_bit(WDT);

	// GPIO on PA0

	// Set pin direction to output
	gpio_set_pin_direction(HOLD_MEM, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(HOLD_MEM,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	gpio_set_pin_function(HOLD_MEM, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA1

	// Set pin direction to output
	gpio_set_pin_direction(SS0, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(SS0,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	gpio_set_pin_function(SS0, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA2

	// Set pin direction to output
	gpio_set_pin_direction(WP_MEM, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(WP_MEM,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	gpio_set_pin_function(WP_MEM, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA11

	// Set pin direction to input
	gpio_set_pin_direction(ENV_INT, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(ENV_INT,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF);

	gpio_set_pin_function(ENV_INT, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA12

	// Set pin direction to input
	gpio_set_pin_direction(P1_ON, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(P1_ON,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF);

	gpio_set_pin_function(P1_ON, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA13

	// Set pin direction to output
	gpio_set_pin_direction(FLASH_RST, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(FLASH_RST,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	gpio_set_pin_function(FLASH_RST, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA14

	// Set pin direction to output
	gpio_set_pin_direction(RFID_VEN, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(RFID_VEN,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	gpio_set_pin_function(RFID_VEN, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA16

	// Set pin direction to input
	gpio_set_pin_direction(ENDSW2, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(ENDSW2,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF); //OFF

	gpio_set_pin_function(ENDSW2, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA18

	// Set pin direction to input
	gpio_set_pin_direction(ENDSW1, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(ENDSW1,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF); //OFF

	gpio_set_pin_function(ENDSW1, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA19

	// Set pin direction to output
	gpio_set_pin_direction(GSM_RST, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(GSM_RST,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	gpio_set_pin_function(GSM_RST, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA20

	// Set pin direction to output
	gpio_set_pin_direction(GSM_PON, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(GSM_PON,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	gpio_set_pin_function(GSM_PON, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA21

	// Set pin direction to output
	gpio_set_pin_direction(BT_WU, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(BT_WU,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	gpio_set_pin_function(BT_WU, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA22

	// Set pin direction to output
	gpio_set_pin_direction(BT_RST, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(BT_RST,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	gpio_set_pin_function(BT_RST, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA23

	// Set pin direction to output
	gpio_set_pin_direction(LEDSW1, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(LEDSW1,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	gpio_set_pin_function(LEDSW1, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA24

	// Set pin direction to output
	gpio_set_pin_direction(LEDSW2, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(LEDSW2,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	gpio_set_pin_function(LEDSW2, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA25

	// Set pin direction to output
	gpio_set_pin_direction(CTS0, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(CTS0,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	gpio_set_pin_function(CTS0, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA26

	// Set pin direction to output
	gpio_set_pin_direction(RTS0, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(RTS0,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	gpio_set_pin_function(RTS0, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA27

	// Set pin direction to input
	gpio_set_pin_direction(MOV_INT, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(MOV_INT,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF);

	gpio_set_pin_function(MOV_INT, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA28

	// Set pin direction to input
	gpio_set_pin_direction(CTS, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(CTS,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF);

	gpio_set_pin_function(CTS, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA29

	// Set pin direction to input
	gpio_set_pin_direction(RTS, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(RTS,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF);

	gpio_set_pin_function(RTS, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA30

	// Set pin direction to input
	gpio_set_pin_direction(DOOR_SW, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(DOOR_SW,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_UP);

	gpio_set_pin_function(DOOR_SW, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA31

	// Set pin direction to input
	gpio_set_pin_direction(RFID_IRQ, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(RFID_IRQ,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF);

	gpio_set_pin_function(RFID_IRQ, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PB0

	// Set pin direction to output
	gpio_set_pin_direction(LEDGRN, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(LEDGRN,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	gpio_set_pin_function(LEDGRN, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PB1

	// Set pin direction to output
	gpio_set_pin_direction(LEDYEL, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(LEDYEL,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	gpio_set_pin_function(LEDYEL, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PB4

	// Set pin direction to output
	gpio_set_pin_direction(SS4, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(SS4,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	gpio_set_pin_function(SS4, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PB8

	// Set pin direction to input
	gpio_set_pin_direction(LOW_POWER, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(LOW_POWER,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF);

	gpio_set_pin_function(LOW_POWER, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PB9

	// Set pin direction to input
	gpio_set_pin_direction(IRQ_IR, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(IRQ_IR,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF);

	gpio_set_pin_function(IRQ_IR, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PB10

	// Set pin direction to output
	gpio_set_pin_direction(MOTOR_IO1, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(MOTOR_IO1,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	gpio_set_pin_function(MOTOR_IO1, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PB11

	// Set pin direction to output
	gpio_set_pin_direction(MOTOR_IO2, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(MOTOR_IO2,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	gpio_set_pin_function(MOTOR_IO2, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PB13

	// Set pin direction to output
	gpio_set_pin_direction(MOTOR_IO3, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(MOTOR_IO3,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	gpio_set_pin_function(MOTOR_IO3, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PB14

	// Set pin direction to output
	gpio_set_pin_direction(MOTOR_IO4, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(MOTOR_IO4,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	gpio_set_pin_function(MOTOR_IO4, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PB15

	// Set pin direction to output
	gpio_set_pin_direction(MOTOR_IO5, GPIO_DIRECTION_OUT);

	gpio_set_pin_level(MOTOR_IO5,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	gpio_set_pin_function(MOTOR_IO5, GPIO_PIN_FUNCTION_OFF);

	ADC_0_init();

	delay_driver_init();
	CRC_0_init();

	USART_0_init();

	USART_1_init();

	SPI_0_init();

	I2C_0_init();
}

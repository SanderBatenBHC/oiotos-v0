/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */
#ifndef ATMEL_START_PINS_H_INCLUDED
#define ATMEL_START_PINS_H_INCLUDED

#include <hal_gpio.h>

// SAMG has 4 pin functions

#define GPIO_PIN_FUNCTION_A 0
#define GPIO_PIN_FUNCTION_B 1
#define GPIO_PIN_FUNCTION_C 2
#define GPIO_PIN_FUNCTION_D 3

#define HOLD_MEM GPIO(GPIO_PORTA, 0)
#define SS0 GPIO(GPIO_PORTA, 1)
#define WP_MEM GPIO(GPIO_PORTA, 2)
#define PA3 GPIO(GPIO_PORTA, 3)
#define PA4 GPIO(GPIO_PORTA, 4)
#define PA5 GPIO(GPIO_PORTA, 5)
#define PA6 GPIO(GPIO_PORTA, 6)
#define PA9 GPIO(GPIO_PORTA, 9)
#define PA10 GPIO(GPIO_PORTA, 10)
#define ENV_INT GPIO(GPIO_PORTA, 11)
#define P1_ON GPIO(GPIO_PORTA, 12)
#define FLASH_RST GPIO(GPIO_PORTA, 13)
#define RFID_VEN GPIO(GPIO_PORTA, 14)
#define PA15 GPIO(GPIO_PORTA, 15)
#define ENDSW2 GPIO(GPIO_PORTA, 16)
#define PA17 GPIO(GPIO_PORTA, 17)
#define ENDSW1 GPIO(GPIO_PORTA, 18)
#define GSM_RST GPIO(GPIO_PORTA, 19)
#define GSM_PON GPIO(GPIO_PORTA, 20)
#define BT_WU GPIO(GPIO_PORTA, 21)
#define BT_RST GPIO(GPIO_PORTA, 22)
#define LEDSW1 GPIO(GPIO_PORTA, 23)
#define LEDSW2 GPIO(GPIO_PORTA, 24)
#define CTS0 GPIO(GPIO_PORTA, 25)
#define RTS0 GPIO(GPIO_PORTA, 26)
#define MOV_INT GPIO(GPIO_PORTA, 27)
#define CTS GPIO(GPIO_PORTA, 28)
#define RTS GPIO(GPIO_PORTA, 29)
#define DOOR_SW GPIO(GPIO_PORTA, 30)
#define RFID_IRQ GPIO(GPIO_PORTA, 31)
#define LEDGRN GPIO(GPIO_PORTB, 0)
#define LEDYEL GPIO(GPIO_PORTB, 1)
#define PB2 GPIO(GPIO_PORTB, 2)
#define PB3 GPIO(GPIO_PORTB, 3)
#define SS4 GPIO(GPIO_PORTB, 4)
#define LOW_POWER GPIO(GPIO_PORTB, 8)
#define IRQ_IR GPIO(GPIO_PORTB, 9)
#define MOTOR_IO1 GPIO(GPIO_PORTB, 10)
#define MOTOR_IO2 GPIO(GPIO_PORTB, 11)
#define MOTOR_IO3 GPIO(GPIO_PORTB, 13)
#define MOTOR_IO4 GPIO(GPIO_PORTB, 14)
#define MOTOR_IO5 GPIO(GPIO_PORTB, 15)

#endif // ATMEL_START_PINS_H_INCLUDED

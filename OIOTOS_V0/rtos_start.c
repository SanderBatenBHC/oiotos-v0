/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file or main.c
 * to avoid loosing it when reconfiguring.
 */
#include "atmel_start.h"
#include "rtos_start.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tml.h>
#include <NxpNci.h>
#include <globals.h>

#define u_length 11
#define t_length 15
#define o_length 20
#define c_length 20
#define d_length 9
#define b_length 2
#define l_length 20


//#define Dummy_time 1506343438UL

#define FIXED_LENGHT_OPEN_FIELD 9 //o;9chars
#define FIXED_LENGTH_TIME_FIELD 11 //u;11chars


#define TASK_EXAMPLE_STACK_SIZE (2048 / sizeof(portSTACK_TYPE))
#define TASK_8192_STACK_SIZE (8192 / sizeof(portSTACK_TYPE))
#define TASK_16384_STACK_SIZE (16384 / sizeof(portSTACK_TYPE))
#define TASK_32768_STACK_SIZE (32768 / sizeof(portSTACK_TYPE))
#warning LET OP!
#define TASK_EXAMPLE_STACK_PRIORITY (tskIDLE_PRIORITY + 1)
static TaskHandle_t      xCreatedExampleTask;

enum Action {DoorClosed = 0 ,DoorOpened,DoorStaysClosed,DoorStaysOpen};
typedef enum Action Action;




struct DoorLog
{
    uint32_t CID;
    uint8_t CID_Short;
    Action Act;
};

struct DataLog
{
    uint8_t RecordType[60];
    uint8_t Value[60];
    struct DoorLog OpenCloseLog[60];
    uint32_t TimeStamp[60];
    uint8_t NumOfrecords;
};


struct Data
{
    int PCB_ID;
    //uint32_t Time[30];
    //struct DoorLog Log[30];
    int length_of_data;
    //bool DataFormatted;
    char FormattedData[20][160];
    int NumberOfSMS;
};

struct Data DemoData;

// struct FormattedSMS
// {
//     char FormattedData[160];
//     int NoOfSMS;
// };

//struct FormattedSMS ToSend[10];

unsigned char DiscoveryTechnologies2[] =
{
    MODE_POLL | TECH_PASSIVE_NFCA
    ,MODE_POLL | TECH_PASSIVE_NFCB
    ,MODE_POLL | TECH_PASSIVE_NFCF
#ifdef P2P_SUPPORT
    ,MODE_POLL | TECH_ACTIVE_NFCF
#endif
    ,MODE_LISTEN | TECH_PASSIVE_NFCA
#ifdef P2P_SUPPORT
    ,MODE_LISTEN | TECH_PASSIVE_NFCF
    ,MODE_LISTEN | TECH_ACTIVE_NFCA
    ,MODE_LISTEN | TECH_ACTIVE_NFCF
#endif
};


void DataFormatter(struct Data*);

int DemoSMSRun; //Counter for the time.

//Semaphores
static SemaphoreHandle_t disp_mutex;
static SemaphoreHandle_t GSM_com;
static SemaphoreHandle_t DemoData_Access;
static SemaphoreHandle_t timer;
static SemaphoreHandle_t I2C;
static SemaphoreHandle_t MOTOR;
static SemaphoreHandle_t SMS;

//Globals
bool GSM_Valid = false; //Protected by GSM_com
bool Test_SMS_Send = false;
bool C_pass = false;
bool N_pass = false;
bool door_IsOpen = false;
bool door_IsClosed = false;
bool statusLED = false;
uint32_t CardID = 0x00;
bool SendData = false;

struct DataLog CurLog;

int cnt = 60;


/**
 * OS example task
 *
 * \param[in] p The void pointer for OS task Standard model.
 *
 */
// static void example_task(void *p)
// {
// 	(void)p;
// 	while (1) {
// 		if (xSemaphoreTake(disp_mutex, ~0)) {
// 			/* add your code */
// 			gpio_toggle_pin_level(LEDGRN);
// 			xSemaphoreGive(disp_mutex);
// 		}
// 		os_sleep(500);
// 	}
// }

static void example_task(void *p)
{
    (void)p;


    bool sw1, sw2;
    while (1)
        {
            if (xSemaphoreTake(disp_mutex, ~0))
                {
                    /* add your code */
                    sw1 = gpio_get_pin_level(ENDSW1);
                    sw2 = gpio_get_pin_level(ENDSW2);
                    //gpio_set_pin_level(LEDGRN,!sw1);
                    //gpio_set_pin_level(LEDYEL,!sw2);

                    uint8_t wm = uxTaskGetStackHighWaterMark(NULL);
                    char wmb[10];
                    sprintf(wmb,"Example Task: %d",wm);
                    //SWO_print_string(0,wmb);


                    //bool pin = gpio_get_pin_level(DOOR_SW);
                    //gpio_set_pin_level(LEDGRN,pin);


                    xSemaphoreGive(disp_mutex);

                }

            os_sleep(10);
        }
}

static void MOTOR_CONTROL(void *p)
{
    (void)p;


    bool sw1, sw2;
    while (1)
        {
            if (xSemaphoreTake(MOTOR, ~0))
                {
                    if (door_IsClosed)
                        {
                            if ((C_pass == TRUE) || (N_pass == TRUE))
                                {
                                    CurLog.OpenCloseLog[CurLog.NumOfrecords].Act = DoorOpened;
                                    CurLog.OpenCloseLog[CurLog.NumOfrecords].CID = CardID;
                                    CurLog.RecordType[CurLog.NumOfrecords] = 'o';
                                    CurLog.TimeStamp[CurLog.NumOfrecords] = LocalTime;
                                    CurLog.NumOfrecords++;

                                    /* add your code */

                                    // OPEN DOOR
                                    // Totdat achter switch flapje ziet...
                                    //of totdat timeout
                                    //sw1 = gpio_get_pin_level(ENDSW1);
                                    //sw2 = gpio_get_pin_level(ENDSW2);
                                    os_lock();
                                    gpio_set_pin_level(MOTOR_IO1,HIGH); //nSleep
                                    gpio_set_pin_level(MOTOR_IO2,HIGH);  //IN1
                                    gpio_set_pin_level(MOTOR_IO3,HIGH); //EN1
                                    gpio_set_pin_level(MOTOR_IO4,LOW); //IN2
                                    gpio_set_pin_level(MOTOR_IO5,HIGH); //EN2

                                    while(gpio_get_pin_level(ENDSW2) == true)
                                        {
                                            delay_ms(1);
                                        }

                                    gpio_set_pin_level(MOTOR_IO1,HIGH); //nSleep
                                    gpio_set_pin_level(MOTOR_IO2,HIGH);  //IN1
                                    gpio_set_pin_level(MOTOR_IO3,HIGH); //EN1
                                    gpio_set_pin_level(MOTOR_IO4,HIGH); //IN2
                                    gpio_set_pin_level(MOTOR_IO5,HIGH); //EN2
                                    os_unlock();

                                    delay_ms(10);
                                    door_IsOpen = true;
                                    door_IsClosed = false;
                                    NxpNci_StopDiscovery();
                                    //delay_ms(5000);
                                    os_sleep(5000);
                                    C_pass = false;
                                    N_pass = false;





                                    uint8_t wm = uxTaskGetStackHighWaterMark(NULL);
                                    char wmb[10];
                                    sprintf(wmb,"Motor control: %d",wm);
                                    SWO_print_string(0,wmb);
                                }

                        }
                    else if (door_IsOpen)
                        {
                            //Check if it can be closed
                            //Door switch
                            if (gpio_get_pin_level(DOOR_SW) == LOW)
                                {
                                    delay_ms(100);
                                    if (gpio_get_pin_level(DOOR_SW) == LOW)
                                        {
                                            os_lock();
                                            CurLog.RecordType[CurLog.NumOfrecords] = 'c';
                                            CurLog.TimeStamp[CurLog.NumOfrecords] = LocalTime;
                                            CurLog.OpenCloseLog[CurLog.NumOfrecords].CID = 0x00;
                                            CurLog.OpenCloseLog[CurLog.NumOfrecords].Act = DoorClosed;
                                            CurLog.NumOfrecords++;

                                            gpio_set_pin_level(MOTOR_IO1,HIGH); //nSleep
                                            gpio_set_pin_level(MOTOR_IO2,LOW);  //IN1
                                            gpio_set_pin_level(MOTOR_IO3,HIGH); //EN1
                                            gpio_set_pin_level(MOTOR_IO4,HIGH); //IN2
                                            gpio_set_pin_level(MOTOR_IO5,HIGH); //EN2

                                            while(gpio_get_pin_level(ENDSW1) == true)
                                                {
                                                    delay_ms(1);
                                                }

                                            gpio_set_pin_level(MOTOR_IO1,HIGH); //nSleep
                                            gpio_set_pin_level(MOTOR_IO2,HIGH);  //IN1
                                            gpio_set_pin_level(MOTOR_IO3,HIGH); //EN1
                                            gpio_set_pin_level(MOTOR_IO4,HIGH); //IN2
                                            gpio_set_pin_level(MOTOR_IO5,HIGH); //EN2
                                            os_unlock();
                                            delay_ms(100);
                                            door_IsClosed = true;
                                            door_IsOpen = false;
                                            NxpNci_StopDiscovery();
                                            NxpNci_StartDiscovery(DiscoveryTechnologies2,sizeof(DiscoveryTechnologies2));
                                        }
                                }


                        }
                    uint8_t wm = uxTaskGetStackHighWaterMark(NULL);
                    char wmb[10];
                    sprintf(wmb,"Motor control: %d",wm);
                    //SWO_print_string(0,wmb);
                    xSemaphoreGive(MOTOR);

                }

            os_sleep(10);
        }
}

static void RFID_READER(void *p)
{
    (void)p;
    while (1)
        {
            if (xSemaphoreTake(I2C,~0))
                {

                    //START
                    bool validRead = false;
                    // Forever loop //-> To RTOS TASK

                    //PRINTF("\r\nWAITING FOR DEVICE DISCOVERY\r\n");

                    /* Wait until a peer is discovered */
                    gpio_set_pin_level(LEDYEL,LOW);
                    //gpio_set_pin_level(LEDGRN,LOW);
                    //NxpNci_WaitForDiscoveryNotification(&RfInterface);
                    //validRead = true;
                    os_lock();
                    validRead = NxpNci_PollDiscoveryNotification(&RfInterface);
                    os_unlock();
#warning purge data on empty receive
#ifdef CARDEMU_SUPPORT
                    if ((RfInterface.Interface == INTF_ISODEP) && (RfInterface.ModeTech == (MODE_LISTEN | TECH_PASSIVE_NFCA)))
                        {
                            //PRINTF("\r\n - LISTEN MODE: Activated from remote Reader\r\n");
                            NxpNci_Process(NXPNCI_MODE_CARDEMU, RfInterface);
                            //PRINTF("\r\nREADER DISCONNECTED\n");
                        }
                    else
#endif
#ifdef P2P_SUPPORT
                        if (RfInterface.Interface == INTF_NFCDEP)
                            {
                                /* Is target mode ? */
                                if ((RfInterface.ModeTech & MODE_LISTEN) == MODE_LISTEN) ;//PRINTF("\r\n - P2P TARGET MODE: Activated from remote Initiator\r\n");
                                else //PRINTF("\r\n - P2P INITIATOR MODE: Remote Target activated\r\n");
                                    NxpNci_Process(NXPNCI_MODE_P2P, RfInterface);
                                //PRINTF("\r\nPEER LOST\r\n");
                            }
                        else
#endif
#ifdef RW_SUPPORT
                            if (validRead == true)
                                {

                                    if ((RfInterface.ModeTech & MODE_MASK) == MODE_POLL)
                                        {
                                            if ((RfInterface.Protocol != PROT_NFCDEP) && (RfInterface.Interface != INTF_UNDETERMINED))
                                                {
                                                    //PRINTF("\r\n - POLL MODE: Remote T%dT activated\r\n", RfInterface.Protocol);
                                                    //gpio_set_pin_level(LEDYEL,HIGH);
                                                    NxpNci_Process(NXPNCI_MODE_RW, RfInterface); //MIFARE!
                                                    os_lock();
                                                    NxpNci_StopDiscovery();
                                                    NxpNci_StartDiscovery(DiscoveryTechnologies2,sizeof(DiscoveryTechnologies2));
                                                    //NxpNci_RestartDiscovery();
                                                    os_unlock();
                                                    gpio_set_pin_level(LEDYEL,HIGH);

                                                }
                                            else
                                                {
                                                    gpio_set_pin_level(LEDYEL,LOW);
                                                    //PRINTF("\r\n - POLL MODE: Undetermined target\r\n");
                                                    /* Restart discovery loop */
                                                    os_lock();
                                                    NxpNci_StopDiscovery();
                                                    NxpNci_StartDiscovery(DiscoveryTechnologies2,sizeof(DiscoveryTechnologies2));
                                                    //NxpNci_RestartDiscovery();
                                                    os_unlock();


                                                }

                                            //PRINTF("\r\nCARD DISCONNECTED\r\n");
                                        }
                                    else
#endif
                                        {
                                            gpio_set_pin_level(LEDYEL,LOW);
                                            //PRINTF("\r\nWRONG DISCOVERY\r\n");
                                            /*os_lock();
                                            NxpNci_StopDiscovery();
                                            NxpNci_StartDiscovery(DiscoveryTechnologies2,sizeof(DiscoveryTechnologies2));
                                            //NxpNci_RestartDiscovery();
                                            os_unlock();*/
                                            os_lock();
                                            NxpNci_StopDiscovery();
                                            NxpNci_StartDiscovery(DiscoveryTechnologies2,sizeof(DiscoveryTechnologies2));
                                            os_unlock();


                                        }

                                }



                    //END

                    uint8_t wm = uxTaskGetStackHighWaterMark(NULL);
                    char wmb[10];
                    //sprintf(wmb,"RFID: %d",wm);
                    //SWO_print_string(0,wmb);
                    xSemaphoreGive(I2C);

                }
            os_sleep(200);
        }
}



static void print(void *p)
{
    (void)p;
    while (1)
        {
            if(xSemaphoreTake(disp_mutex,~0))
                {
                    //SWO_print_string(0,"test");
                    uint8_t wm = uxTaskGetStackHighWaterMark(NULL);
                    char wmb[10];
                    sprintf(wmb,"Print: %d",wm);
                    //SWO_print_string(0,wmb);
                    xSemaphoreGive(disp_mutex);
                    LocalTime++;


                }
            os_sleep(1000);
        }
}

static void TimerInc(void *p)
{
    (void)p;
    while (1)
        {

            if(xSemaphoreTake(timer,~0))
                {
                    //TimerInc does not increase timer anymore :)
                    uint8_t wm = uxTaskGetStackHighWaterMark(NULL);
                    char wmb[10];
                    sprintf(wmb,"TimerInc: %d, Time: %d",wm,LocalTime);
                    //SWO_print_string(0,wmb);
                    if (door_IsOpen) statusLED = false;
                    gpio_set_pin_level(LEDGRN,statusLED);
                    statusLED = !statusLED;


                    xSemaphoreGive(timer);
                }
            if (statusLED == HIGH)
                os_sleep(3000);
            else
                os_sleep(20);
        }
}

static void door_switch(void *p)
{
    (void)p;
    bool pin;
    while (1)
        {
            if (xSemaphoreTake(disp_mutex, ~0))
                {
                    /* add your code */
                    //pin = gpio_get_pin_level(DOOR_SW);
                    //gpio_set_pin_level(LEDYEL,pin);
                    uint8_t wm = uxTaskGetStackHighWaterMark(NULL);
                    char wmb[10];
                    sprintf(wmb,"Door Switch: %d",wm);
                    //SWO_print_string(0,wmb);
                    xSemaphoreGive(disp_mutex);
                }
            os_sleep(10);
        }
}

static void GSM_detect(void *p)
{
    (void)p;
    uint8_t rx_buf[10] = "emtpy";
    while (1)
        {
            if (xSemaphoreTake(SMS, ~0))
                {
                    os_lock();
                    SWO_print_string(0,"Try to detect GSM Module...");
                    struct io_descriptor *io, *io2;
                    usart_sync_get_io_descriptor(&USART_0, &io);
                    usart_sync_enable(&USART_0);
                    io_write(io, (uint8_t *)"AT", 2);
// 			while(!usart_sync_is_rx_not_empty(&USART_0))
// 			;
// 			io_read(io,&rx_buf[0],1);
// 			while(!usart_sync_is_rx_not_empty(&USART_0))
// 			;
// 			io_read(io,&rx_buf[1],1);

                    SWO_print_string(0,rx_buf);
                    os_unlock();

                    usart_sync_get_io_descriptor(&USART_1, &io2);
                    usart_sync_enable(&USART_1);
                    io_write(io2, (uint8_t *)"AT", 2);

                    uint8_t wm = uxTaskGetStackHighWaterMark(NULL);
                    char wmb[10];
                    sprintf(wmb,"GSM Detect: %d",wm);
                    SWO_print_string(0,wmb);
                    xSemaphoreGive(SMS);
                }
            os_sleep(600);
        }
}

// static void DemoSMS(void *p)
// {
//     (void)p;
//     while (1)
//         {
//             if(xSemaphoreTake(disp_mutex,~0))
//                 {
//
//                     SWO_print_string(0,"Creating Demo Data...");
//                     DemoData.DataFormatted = false;
//                     DemoData.length_of_data = 0;
//                     DemoData.PCB_ID = 0x00000002;
//                     //LocalTime += 10;
//                     DemoData.Log[DemoData.length_of_data].Act = DoorOpened;
//                     DemoData.Log[DemoData.length_of_data].CID = 0x4E2FC1D6;
//                     DemoData.Time[DemoData.length_of_data] = LocalTime+1;
//                     DemoData.length_of_data++;
//                     DemoData.Log[DemoData.length_of_data].Act = DoorClosed;
//                     DemoData.Log[DemoData.length_of_data].CID = 0x4E2FC1D6;
//                     DemoData.Time[DemoData.length_of_data] = LocalTime+2;
//                     DemoData.length_of_data++;
//                     DemoData.Log[DemoData.length_of_data].Act = DoorOpened;
//                     DemoData.Log[DemoData.length_of_data].CID = 0x4E6BA9D6;
//                     DemoData.Time[DemoData.length_of_data] = LocalTime+3;
//                     DemoData.length_of_data++;
//                     DemoData.Log[DemoData.length_of_data].Act = DoorClosed;
//                     DemoData.Log[DemoData.length_of_data].CID = 0x4E6BA9D6;
//                     DemoData.Time[DemoData.length_of_data] = LocalTime+4;
//                     DemoData.length_of_data++;
//
//                     DemoData.DataFormatted=true;
//                     char buffer[100] ="";
//                     char FormattedBuffer[150] = "";
//                     sprintf(buffer, "d%08d",DemoData.PCB_ID); //INCREASE STACK!
//                     strncpy(FormattedBuffer,buffer,9);
//
//                     for (int x=0; x < DemoData.length_of_data; x++)
//                         {
//                             switch (DemoData.Log[x].Act)
//                                 {
//
//                                 case DoorOpened:
//                                     sprintf(buffer,"u%010luo%08lX",DemoData.Time[x] , DemoData.Log[x].CID);
//                                     break;
//                                 case DoorClosed:
//                                     sprintf(buffer,"u%010luc00000000",DemoData.Time[x]);
//                                     break;
//
//                                 }
//                             strncat(FormattedBuffer,buffer,20);
//
//                         }
//                     DemoData.length_of_data = strlen(FormattedBuffer);
//                     strcpy(DemoData.FormattedData,FormattedBuffer);
//                     SWO_print_string(0,DemoData.FormattedData);
//
//
//                     if (cnt == 60)
//                         {
//                             cnt = 0;
//
//                             SWO_print_string(0,"Try to send data...");
//                             struct io_descriptor *io;
//                             usart_sync_get_io_descriptor(&USART_0, &io);
//                             usart_sync_enable(&USART_0);
//                             io_write(io, (uint8_t *)"AT\r", 3);
//                             for (int x=0 ; x < 100000; x++);
//                             io_write(io, (uint8_t *)"AT+CMGF=1\r", 10);
//                             for (int x=0 ; x < 100000; x++);
//                             //io_write(io, (uint8_t *)"at+cmgs=\"310000202\"\r", 20);
//                             //io_write(io, (uint8_t *)"at+cmgs=\"0647775682\"\r", 21);
//                             for (int x=0 ; x < 500000; x++);
//                             io_write(io, (uint8_t *)DemoData.FormattedData,DemoData.length_of_data);
//                             for (int x=0 ; x < 200000; x++);
//                             io_write(io, (uint8_t *)"\032", 1);
//                         }
//                     cnt++;
//
//
//                     uint8_t wm = uxTaskGetStackHighWaterMark(NULL);
//                     char wmb[10];
//                     sprintf(wmb,"Demo SMS: %d",wm);
//                     SWO_print_string(0,wmb);
//                     xSemaphoreGive(disp_mutex);
//                 }
//             os_sleep(1000);
//         }
// }

static void DemoSMS(void *p)
{
    (void)p;
    while (1)
        {
            char wmb[100];
            if(xSemaphoreTake(SMS,~0))
                {

                    if (SendData == true)
                        {
                            SendData = false;
                            bool GPSData = false;
                            uint8_t rx_buf[100] = "emtpy";
                            char fields[10][70];
                            int fieldPointer = 0;
                            int charPointer = 0;
                            int CurSMS = 0;
							int Fixes = 0;

                            os_lock();
                            CurLog.RecordType[CurLog.NumOfrecords] = 'b';
                            CurLog.Value[CurLog.NumOfrecords] = 1;
                            CurLog.TimeStamp[CurLog.NumOfrecords] = LocalTime;
                            CurLog.NumOfrecords++;
//                             CurLog.RecordType[CurLog.NumOfrecords] = 'b';
//                             CurLog.Value[CurLog.NumOfrecords] = 2;
//                             CurLog.TimeStamp[CurLog.NumOfrecords] = LocalTime;
//                             CurLog.NumOfrecords++;
//                             LocalTime += 100;

//                             CurLog.RecordType[CurLog.NumOfrecords] = 'b';
//                             CurLog.Value[CurLog.NumOfrecords] = 3;
//                             CurLog.TimeStamp[CurLog.NumOfrecords] = LocalTime;
//                             CurLog.NumOfrecords++;
//                             LocalTime += 100;
                            os_unlock();
                            os_sleep(1000);
                            os_lock();
                            CurLog.RecordType[CurLog.NumOfrecords] = 't';
                            CurLog.Value[CurLog.NumOfrecords] = 14;
                            CurLog.TimeStamp[CurLog.NumOfrecords] = LocalTime;
                            CurLog.NumOfrecords++;
                            os_unlock();

                            //Fetch time AND GPS....
                            struct io_descriptor *io;
                            usart_sync_get_io_descriptor(&USART_0, &io);
                            usart_sync_enable(&USART_0);

                            io_write(io, (uint8_t *)"AT\r", 3);
                            os_sleep(1000);
                            io_write(io, (uint8_t*)"AT+GPSNMEA=0\r", 13);
                            os_sleep(200);
                            io_write(io, (uint8_t*)"AT+GPSPVT=1,1,1\r", 16);
                            os_sleep(200);
                            io_write(io, (uint8_t*)"AT+GPSSTART=0\r", 14);
                            os_sleep(5000);

//                             while(!usart_sync_is_rx_not_empty(&USART_0))
//                                 ;
                            os_lock();
                            rx_buf [0]= "\0";
                            io_read(io,&rx_buf,30);
                            os_unlock();

                            sprintf(wmb,"Data In: %s",rx_buf);
                            SWO_print_string(0,wmb);

                            while (GPSData == false)
                                {


                                    //while(!usart_sync_is_rx_not_empty(&USART_0))
                                    //    ;
                                    os_lock();
                                    rx_buf [0]= "\0";
                                    io_read(io,&rx_buf,70);
                                    //rx_buf[5] = '\0';
                                    sprintf(wmb,"Data: %s",rx_buf);
                                    SWO_print_string(0,wmb);


                                    for (int p = 0; p < 70; p++)
                                        {
                                            if (rx_buf[p] == ',')
                                                {
                                                    fields[fieldPointer][charPointer] = '\0';
                                                    fieldPointer++;
                                                    charPointer = 0;

                                                }
                                            else
                                                {
                                                    fields[fieldPointer][charPointer] = rx_buf[p];
                                                    charPointer++;
                                                }

                                        }
                                    fieldPointer = 0;
                                    charPointer = 0;

                                    sprintf(wmb,"Data In: %s",rx_buf);
                                    SWO_print_string(0,wmb);
                                    sprintf(wmb,"Msg type: %s",fields[0]);
                                    SWO_print_string(0,wmb);
                                    sprintf(wmb,"Time: %s",fields[1]);
                                    SWO_print_string(0,wmb);
                                    sprintf(wmb,"Date: %s",fields[2]);
                                    SWO_print_string(0,wmb);
                                    sprintf(wmb,"Fix Status: %s",fields[3]);
                                    SWO_print_string(0,wmb);
                                    sprintf(wmb,"Lat: %s",fields[4]);
                                    SWO_print_string(0,wmb);
                                    sprintf(wmb,"Long: %s",fields[5]);
                                    SWO_print_string(0,wmb);
                                    //sprintf(wmb,"Data In6: %s",fields[6]);
                                    //SWO_print_string(0,wmb);
                                    //sprintf(wmb,"Data In7: %s",fields[7]);
                                    //SWO_print_string(0,wmb);
                                    if (strncmp(fields[3],"3D",2) == 0)
                                        {
                                            sprintf(wmb,"Valid fix: %s",fields[3]);
                                            SWO_print_string(0,wmb);
                                            Fixes++;
											if (Fixes > 4) GPSData = true;
                                        }

                                    //GPSData = true;
//#warning Test Data!!
                                    os_unlock();
                                    os_sleep(700);


                                }
                            io_write(io, (uint8_t*)"AT+GPSSTOP\r", 11);
                            os_sleep(1000);

                            //Format the GPS data...
                            char Lat[2], LatD[4], LatM[3], LatS[6];
                            char Long[2], LongD[4], LongM[3], LongS[6];
                            Lat[0] = fields[4][0];
                            Lat[1] = '\0';
                            LatD[0] = fields[4][2];
                            LatD[1] = fields[4][3];
                            LatD[2] = fields[4][4];
                            LatD[3] = '\0';
                            LatM[0] = fields[4][6];
                            LatM[1] = fields[4][7];
                            LatM[2] = '\0';
                            LatS[0] = fields[4][9];
                            LatS[1] = fields[4][10];
                            LatS[2] = fields[4][11];
                            LatS[3] = fields[4][12];
                            LatS[4] = fields[4][13];
                            LatS[5] = '\0';
                            sprintf(wmb,"Valid splitted...: %s %s %s %s",Lat ,LatD, LatM, LatS);
                            SWO_print_string(0,wmb);

                            Long[0] = fields[5][0];
                            Long[1] = '\0';
                            LongD[0] = fields[5][2];
                            LongD[1] = fields[5][3];
                            LongD[2] = fields[5][4];
                            LongD[3] = '\0';
                            LongM[0] = fields[5][6];
                            LongM[1] = fields[5][7];
                            LongM[2] = '\0';
                            LongS[0] = fields[5][9];
                            LongS[1] = fields[5][10];
                            LongS[2] = fields[5][11];
                            LongS[3] = fields[5][12];
                            LongS[4] = fields[5][13];
                            LongS[5] = '\0';
                            sprintf(wmb,"Valid splitted...: %s %s %s %s",Long ,LongD, LongM, LongS);
                            SWO_print_string(0,wmb);

                            volatile int iLat, iLatD, iLatM, iLatS;
                            volatile int iLong, iLongD, iLongM, iLongS;

                            iLat = atoi(Lat);
                            iLatD = atoi(LatD);
                            iLatM = atoi(LatM);
                            iLatS = atoi(LatS);

                            iLong = atoi(Long);
                            iLongD = atoi(LongD);
                            iLongM = atoi(LongM);
                            iLongS = atoi(LongS);

//                             while(1)
//                                 {
//                                     //ENDLESS!
//                                 }

                            volatile float LatDD,LongDD;
                            LatDD = (float)((float)iLatD + ((float)iLatM/60) + ((float)iLatS/3600));
                            LongDD = (float)((float)iLongD + ((float)iLongM/60) + ((float)iLongS/3600));
                            uint32_t iLatDD = LatDD * 1000000;
                            uint32_t iLongDD = LongDD * 100000;

                            CurLog.RecordType[CurLog.NumOfrecords] = 'l';
                            CurLog.TimeStamp[CurLog.NumOfrecords] = LocalTime;
                            CurLog.NumOfrecords++;

                            SWO_print_string(0,"Formatting the data....");
                            DemoData.PCB_ID = 0x00000003;
                            char buffer[100] ="";
                            char buffer2[100] ="";
                            char FormattedBuffer[160] = "";
                            uint8_t charsWritten = 0;
                            int FieldLenght = 0;



                            sprintf(buffer, "d%08d",DemoData.PCB_ID); //INCREASE STACK!
                            strncpy(FormattedBuffer,buffer,9);
                            for (int x=0; x < CurLog.NumOfrecords; x++)
                                {
                                    switch (CurLog.RecordType[x])
                                        {

                                        case 'o':
                                            charsWritten = sprintf(buffer,"u%010luo%08lX",CurLog.TimeStamp[x] , CurLog.OpenCloseLog[x].CID);

                                            break;
                                        case 'c':
                                            charsWritten = sprintf(buffer,"u%010luc%08lX",CurLog.TimeStamp[x], CurLog.OpenCloseLog[x].CID);

                                            break;
                                        case 'b':
                                            charsWritten = sprintf(buffer,"u%010lub%d",CurLog.TimeStamp[x],CurLog.Value[x]);

                                            break;
                                        case 't':
                                            charsWritten = sprintf(buffer, "u%010lut%+03d",CurLog.TimeStamp[x],CurLog.Value[x] );

                                            break;
                                        case 'l':
                                            charsWritten = sprintf(buffer,"u%010lul%+09d,%+09d",LocalTime,iLatDD, iLongDD);

                                            break;

                                        }
                                    //Check for too long! Charswritte has to fit, else increase SMS message.
                                    // If new message, add d identifier!
                                    if (charsWritten > (159 - strlen(FormattedBuffer)))
                                        {
                                            strcpy(DemoData.FormattedData[CurSMS],FormattedBuffer);
                                            CurSMS++;
											if (CurSMS > 19) CurSMS = 19;
                                            //FormattedBuffer = ""              ;
                                            memset (FormattedBuffer,'\0', 160);
                                            sprintf(buffer2, "d%08d",DemoData.PCB_ID); //INCREASE STACK!
                                            strncpy(FormattedBuffer,buffer2,9);
                                            strncat(FormattedBuffer,buffer,charsWritten);
                                        }
                                    else
                                        {
                                            strncat(FormattedBuffer,buffer,charsWritten);
                                        }


                                    DemoData.length_of_data += charsWritten;

                                }


                            strcpy(DemoData.FormattedData[CurSMS],FormattedBuffer);

                            sprintf(wmb,"Demo SMS, current %d, Logs: %d",CurSMS, CurLog.NumOfrecords);
                            SWO_print_string(0,wmb);
                            //strncat(FormattedBuffer,buffer,charsWritten);


                            //DemoData.length_of_data = strlen(FormattedBuffer);
                            //strcpy(DemoData.FormattedData[CurSMS],FormattedBuffer);

                            SWO_print_string(0,DemoData.FormattedData[0]);
                            SWO_print_string(0,DemoData.FormattedData[1]);
                            SWO_print_string(0,DemoData.FormattedData[2]);
                            SWO_print_string(0,DemoData.FormattedData[3]);

//Loop through SMS, check the lenghts!
                            for (int sms = 0; sms < (CurSMS+1); sms++)
                                {
//cnt = 0;
                                    os_lock();
                                    SWO_print_string(0,"Try to send data...");
                                    io_write(io, (uint8_t *)"AT\r", 3);
                                    for (int x=0 ; x < 100000; x++);
                                    io_write(io, (uint8_t *)"AT+CMGF=1\r", 10);
                                    for (int x=0 ; x < 100000; x++);
                                    io_write(io, (uint8_t *)"at+cmgs=\"310000202\"\r", 20); //->VALID VODAFONE
                                    //io_write(io, (uint8_t *)"at+cmgs=\"0647775682\"\r", 21);
                                    for (int x=0 ; x < 500000; x++);
                                    io_write(io, (uint8_t *)DemoData.FormattedData[sms],strlen(DemoData.FormattedData[sms]));
                                    for (int x=0 ; x < 200000; x++);
                                    io_write(io, (uint8_t *)"\032", 1);
                                    io_write(io, (uint8_t *)"AT+CCLK?\r", 9);

                                    os_unlock();
                                    os_sleep(10000);
                                }
                            //cnt++;
                            CurLog.NumOfrecords = 0;
                            CurSMS = 0;
                        }


                    uint8_t wm = uxTaskGetStackHighWaterMark(NULL);

                    sprintf(wmb,"Demo SMS: %d, Logs: %d",wm, CurLog.NumOfrecords);
                    SWO_print_string(0,wmb);
                    xSemaphoreGive(SMS);
                }
            os_sleep(1000);
        }
}


// static void GSM_send_test(void *p)
// {
// 	(void)p;
//
// 	while (1)
// 	{
// 		if (xSemaphoreTake(GSM_com, ~0)) {
// 			if (Test_SMS_Send == false)
// 			{
// 				Test_SMS_Send = true;
//
// 				SWO_print_string(0,"Try to send data...");
// 				struct io_descriptor *io;
// 				usart_sync_get_io_descriptor(&USART_0, &io);
// 				usart_sync_enable(&USART_0);
// 				io_write(io, (uint8_t *)"AT\r", 3);
//  				delay_ms(50);
//  				io_write(io, (uint8_t *)"AT+CMGF=1\r", 10);
// 				delay_ms(200);
// 				io_write(io, (uint8_t *)"at+cmgs=\"310000202\"\r", 20);
// 				delay_ms(500);
// 				io_write(io, (uint8_t *)"d2941RRRRRRRRu1502116043t+21ll-50.75450,-179.12345u1502116066o4E71C3D6u1502116078c00000000b1p001P001",100 );
// 				delay_ms(200);
// 				io_write(io, (uint8_t *)"\032", 1);
// 			}
// 			xSemaphoreGive(GSM_com);
// 		}
// 		os_sleep(1000);
// 	}
// }
//RTOS start
//Define tasks here

/*
 * Example
 */
void FREERTOS_V823_0_example(void)
{
    disp_mutex = xSemaphoreCreateMutex();
    GSM_com = xSemaphoreCreateMutex();
    DemoData_Access = xSemaphoreCreateMutex();
    timer = xSemaphoreCreateMutex();
    I2C = xSemaphoreCreateMutex();
    MOTOR = xSemaphoreCreateMutex();
    SMS = xSemaphoreCreateMutex();

    if (disp_mutex == NULL)
        {
            while (1)
                {
                    ;
                }
        }

    if (xTaskCreate(example_task, "Example", TASK_EXAMPLE_STACK_SIZE, NULL, TASK_EXAMPLE_STACK_PRIORITY, xCreatedExampleTask)
            != pdPASS)
        {
            while (1)
                {
                    ;
                }
        }

    if (xTaskCreate(print, "Printer", TASK_EXAMPLE_STACK_SIZE, NULL, 5, NULL)
            != pdPASS)
        {
            while (1)
                {
                    ;
                }
        }

//     if (xTaskCreate(GSM_detect, "GSM_d", TASK_EXAMPLE_STACK_SIZE, NULL, 2, NULL)
//             != pdPASS)
//         {
//             while (1)
//                 {
//                     ;
//                 }
//         }

// 		if (xTaskCreate(GSM_send_test, "GSM_s", TASK_EXAMPLE_STACK_SIZE, NULL, 2, xCreatedExampleTask)
// 		!= pdPASS) {
// 			while (1) {
// 				;
// 			}
// 		}

    if (xTaskCreate(DemoSMS, "Demo_SMS", TASK_32768_STACK_SIZE, NULL, 3, NULL)
            != pdPASS)
        {
            while (1)
                {
                    ;
                }
        }

    if (xTaskCreate(TimerInc, "Timer1", TASK_EXAMPLE_STACK_SIZE, NULL, 3, NULL)
            != pdPASS)
        {
            while (1)
                {
                    ;
                }
        }

    if (xTaskCreate(RFID_READER, "RFID Reader", TASK_16384_STACK_SIZE, NULL, 4, NULL)
            != pdPASS)
        {
            while (1)
                {
                    ;
                }
        }

    if (xTaskCreate(MOTOR_CONTROL, "Motor Control", TASK_EXAMPLE_STACK_SIZE, NULL, 3, NULL)
            != pdPASS)
        {
            while (1)
                {
                    ;
                }
        }

    vTaskStartScheduler();

    return;
}

vApplicationStackOverflowHook(TaskHandle_t xTask, char *pcTaskName)
{
    SWO_print_string(0,"Stack overflow");
    return;
}

vApplicationMallocFailedHook()
{
    SWO_print_string(0,"Malloc fail");
    return;
}
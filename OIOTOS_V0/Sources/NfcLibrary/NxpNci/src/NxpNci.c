/*
* =============================================================================
*
*                    Copyright (c), NXP Semiconductors
*
*                       (C)NXP Electronics N.V.2012
*         All rights are reserved. Reproduction in whole or in part is
*        prohibited without the written consent of the copyright owner.
*    NXP reserves the right to make changes without notice at any time.
*   NXP makes no warranty, expressed, implied or statutory, including but
*   not limited to any implied warranty of merchantability or fitness for any
*  particular purpose, or that the use will not infringe any third party patent,
*   copyright or trademark. NXP must not be liable for any loss or damage
*                            arising from its use.
*
* =============================================================================
*/
#include <stdint.h>
#include <string.h>
#include <driver_init.h>
#include <NxpNci.h>
#include <tml.h>
#include <types.h>
#include <globals.h>
#include <passes.h>

#ifdef CARDEMU_SUPPORT
#include <T4T_NDEF_EMU.h>
#endif

#ifdef P2P_SUPPORT
#include <P2P_NDEF.h>
#endif

#ifdef RW_SUPPORT
#include <RW_NDEF.h>
#include <RW_NDEF_T3T.h>
#endif

#ifdef NCI_DEBUG
#define NCI_PRINT(...)      {printf(__VA_ARGS__);}
unsigned short debug_loop;
#define NCI_PRINT_LOOP(x,y) {for(debug_loop=0; debug_loop<y; debug_loop++) printf("%.2x ", x[debug_loop]);}
#else
#define NCI_PRINT(...)
#define NCI_PRINT_LOOP(x,y)
#endif

static BOOLEAN NxpNci_CheckDevPres(void)
{
    uint8_t NCICoreReset[] = {0x20, 0x00, 0x01, 0x01};
    uint8_t Answer[256];
    uint16_t NbBytes = 0;

    tml_Send(NCICoreReset, sizeof(NCICoreReset), &NbBytes);
    NCI_PRINT("NCI >> "); NCI_PRINT_LOOP(NCICoreReset, NbBytes); NCI_PRINT("\n");
    if (NbBytes != sizeof(NCICoreReset)) return NXPNCI_ERROR;
    tml_Receive(Answer, sizeof(Answer), &NbBytes, TIMEOUT_100MS);
    NCI_PRINT("NCI << "); NCI_PRINT_LOOP(Answer,NbBytes); NCI_PRINT("\n");
    if (NbBytes == 0) return NXPNCI_ERROR;

    return NXPNCI_SUCCESS;
}

static BOOLEAN NxpNci_WaitForReception(uint8_t *pRBuff, uint16_t RBuffSize, uint16_t *pBytesread, uint16_t timeout)
{
    tml_Receive(pRBuff, RBuffSize, pBytesread, timeout);
    NCI_PRINT("NCI << "); NCI_PRINT_LOOP(pRBuff,*pBytesread); NCI_PRINT("\n");
    if (*pBytesread == 0) return NXPNCI_ERROR;

    return NXPNCI_SUCCESS;
}

static BOOLEAN NxpNci_HostTransceive(uint8_t *pTBuff, uint16_t TbuffLen, uint8_t *pRBuff, uint16_t RBuffSize, uint16_t *pBytesread)
{
    tml_Send(pTBuff, TbuffLen, pBytesread);
    NCI_PRINT("NCI >> "); NCI_PRINT_LOOP(pTBuff, *pBytesread); NCI_PRINT("\n");
    if (*pBytesread != TbuffLen) return NXPNCI_ERROR;
    tml_Receive(pRBuff, RBuffSize, pBytesread, TIMEOUT_100MS);
    NCI_PRINT("NCI << "); NCI_PRINT_LOOP(pRBuff,*pBytesread); NCI_PRINT("\n");
    if (*pBytesread == 0) return NXPNCI_ERROR;

    return NXPNCI_SUCCESS;
}

#ifdef CARDEMU_SUPPORT
static void NxpNci_CardMode(NxpNci_RfIntf_t RfIntf)
{
    uint8_t Answer[256];
    uint16_t AnswerSize;

    /* Reset Card emulation state */
    T4T_NDEF_EMU_Reset();

    while(NxpNci_WaitForReception(Answer, sizeof(Answer), &AnswerSize, TIMEOUT_2S) == NXPNCI_SUCCESS)
        {
            /* is RF_DEACTIVATE_NTF ? */
            if((Answer[0] == 0x61) && (Answer[1] == 0x06))
                {
                    /* Come back to discovery state */
                    break;
                }
            /* is DATA_PACKET ? */
            else if((Answer[0] == 0x00) && (Answer[1] == 0x00))
                {
                    /* DATA_PACKET */
                    uint8_t Cmd[256];
                    uint16_t CmdSize;

                    T4T_NDEF_EMU_Next(&Answer[3], Answer[2], &Cmd[3], (unsigned short *) &CmdSize);

                    Cmd[0] = 0x00;
                    Cmd[1] = (CmdSize & 0xFF00) >> 8;
                    Cmd[2] = CmdSize & 0x00FF;

                    NxpNci_HostTransceive(Cmd, CmdSize+3, Answer, sizeof(Answer), &AnswerSize);
                }
        }
}
#endif

#ifdef P2P_SUPPORT
static void NxpNci_P2pMode(NxpNci_RfIntf_t RfIntf)
{
    uint8_t Answer[256];
    uint16_t AnswerSize;

    uint8_t NCILlcpSymm[] = {0x00, 0x00, 0x02, 0x00, 0x00};
    uint8_t NCIRestartDiscovery[] = {0x21, 0x06, 0x01, 0x03};

    /* Reset P2P_NDEF state */
    P2P_NDEF_Reset();

    /* Is Initiator mode ? */
    if((RfIntf.ModeTech & MODE_LISTEN) != MODE_LISTEN)
        {
            /* Initiate communication (SYMM PDU) */
            NxpNci_HostTransceive(NCILlcpSymm, sizeof(NCILlcpSymm), Answer, sizeof(Answer), &AnswerSize);
        }

    /* Get frame from remote peer */
    while(NxpNci_WaitForReception(Answer, sizeof(Answer), &AnswerSize, TIMEOUT_2S) == NXPNCI_SUCCESS)
        {
            /* is DATA_PACKET ? */
            if((Answer[0] == 0x00) && (Answer[1] == 0x00))
                {
                    uint8_t Cmd[256];
                    uint16_t CmdSize;

                    P2P_NDEF_Next(&Answer[3], Answer[2], &Cmd[3], (unsigned short *) &CmdSize);

                    /* Compute DATA_PACKET to answer */
                    Cmd[0] = 0x00;
                    Cmd[1] = (CmdSize & 0xFF00) >> 8;
                    Cmd[2] = CmdSize & 0x00FF;

                    NxpNci_HostTransceive(Cmd, CmdSize+3, Answer, sizeof(Answer), &AnswerSize);
                }
            /* is CORE_INTERFACE_ERROR_NTF ?*/
            else if ((Answer[0] == 0x60) && (Answer[1] == 0x08))
                {
                    break;
                }
            /* is RF_DEACTIVATE_NTF ? */
            else if((Answer[0] == 0x61) && (Answer[1] == 0x06))
                {
                    /* Come back to discovery state */
                    break;
                }
        }

    /* Is Initiator mode ? */
    if((RfIntf.ModeTech & MODE_LISTEN) != MODE_LISTEN)
        {
            /* Communication ended, restart discovery loop */
            NxpNci_HostTransceive(NCIRestartDiscovery, sizeof(NCIRestartDiscovery), Answer, sizeof(Answer), &AnswerSize);
            NxpNci_WaitForReception(Answer, sizeof(Answer), &AnswerSize, TIMEOUT_2S);
        }
}
#endif

#ifdef RW_SUPPORT
BOOLEAN NxpNci_ReaderTagCmd (unsigned char *Command, unsigned char CommandSize, unsigned char *Answer, unsigned char *AnswerSize)
{
    BOOLEAN status = NXPNCI_ERROR;
    uint8_t Cmd[256];
    uint8_t Ans[256];
    uint16_t AnsSize;

    /* Compute and send DATA_PACKET */
    Cmd[0] = 0x00;
    Cmd[1] = 0x00;
    Cmd[2] = CommandSize;
    memcpy(&Cmd[3], Command, CommandSize);
    NxpNci_HostTransceive(Cmd, CommandSize+3, Ans, sizeof(Ans), &AnsSize);

    /* Wait for Answer */
    NxpNci_WaitForReception(Ans, sizeof(Ans), &AnsSize, TIMEOUT_2S);

    if ((Ans[0] == 0x0) && (Ans[1] == 0x0))
        {
            status = NXPNCI_SUCCESS;
        }

    *AnswerSize = Ans[2];
    memcpy(Answer, &Ans[3], *AnswerSize);

    return status;
}

static void NxpNci_ReaderMode(NxpNci_RfIntf_t RfIntf)
{
    uint8_t Answer[256];
    uint16_t AnswerSize;
    uint8_t Cmd[256];
    uint16_t CmdSize = 0;



    uint8_t NCIPresCheckT1T[] = {0x00, 0x00, 0x07, 0x78, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    uint8_t NCIPresCheckT2T[] = {0x00, 0x00, 0x02, 0x30, 0x00};
    uint8_t NCIPollingCmdT3T[] = {0x21, 0x08, 0x04, 0x12, 0xFC, 0x00, 0x01};
    uint8_t NCIPresCheckIsoDep[] = {0x2F, 0x11, 0x00};
    uint8_t NCIRestartDiscovery[] = {0x21, 0x06, 0x01, 0x03};

    RW_NDEF_Reset(RfIntf.Protocol); //(==NULL?)

    /* In case of T3T tag, retrieve card IDm */
    if (RfIntf.Protocol == PROT_T3T)
        {
            NxpNci_HostTransceive(NCIPollingCmdT3T, sizeof(NCIPollingCmdT3T), Answer, sizeof(Answer), &AnswerSize);
            NxpNci_WaitForReception(Answer, sizeof(Answer), &AnswerSize, TIMEOUT_100MS);
            if ((Answer[0] == 0x61) && (Answer[1] == 0x08) && (Answer[3] == 0x00))
                {
                    RW_NDEF_T3T_SetIDm(&Answer[6]);
                }
            else
                {
                    //goto RestartDiscovery;
                }
        }

    while(1)
        {
            uint8_t valid = 0;
            RW_NDEF_Read_Next(&Answer[3], Answer[2], &Cmd[3], (unsigned short *) &CmdSize); //Can be erased? (==null?)
            gpio_set_pin_level(LEDYEL,HIGH);
			//ID CHECK LOOP....
//             if (Answer[37] == 0x90)
//                 if (Answer[38] == 0x6e)
//                     if (Answer[39] == 0x79)
//                         if (Answer[40] == 0x5d)
//                             //gpio_set_pin_level(LEDGRN, HIGH);
//                             valid = 1;

			
			
			for (int x = 0; x < sizeof(cards); x++)
			{
				if (cards[x] == Answer[53])
					if (cards[x+1] == Answer[54])
						if (cards[x+2] == Answer[55])
							if (cards[x+3] == Answer[56])
							{
								valid = 1;
								if (cards[x+4] == Npass) N_pass = true;
								else if (cards[x+4] == Cpass) { C_pass = true;  SendData = true; }
							}
				
			}

            if ((valid == 1) && (door_IsClosed))
                {
                   gpio_set_pin_level(LEDGRN,HIGH);
				   //C_pass = true;
				   //REPLACE WITH DYNAMIC
				   
				   
				   CardID = 0x00000000;
				   CardID = ((Answer[53] << 24) + (Answer[54] << 16) + (Answer[55] << 8) + Answer[56]);
		
				   
                }
            else
                {
					 C_pass = false;
					 N_pass = false;
                    gpio_set_pin_level(LEDGRN,LOW);
                }
				


            if(CmdSize == 0)
                {
                    /* End of the Read operation */
                    break;
                }
            else
                {
                    /* Compute and send DATA_PACKET */
                    Cmd[0] = 0x00;
                    Cmd[1] = (CmdSize & 0xFF00) >> 8;
                    Cmd[2] = CmdSize & 0x00FF;

                    NxpNci_HostTransceive(Cmd, CmdSize+3, Answer, sizeof(Answer), &AnswerSize);
                    NxpNci_WaitForReception(Answer, sizeof(Answer), &AnswerSize, TIMEOUT_1S);
                }
        }

    /* Read Operation ended, perform card presence check */
   
#warning DelayTesting HERE!
//RestartDiscovery:

    /* Communication ended, restart discovery loop */
    //NxpNci_HostTransceive(NCIRestartDiscovery, sizeof(NCIRestartDiscovery), Answer, sizeof(Answer), &AnswerSize);
    //NxpNci_WaitForReception(Answer, sizeof(Answer), &AnswerSize, TIMEOUT_1S);
	//NxpNci_StopDiscovery();

}
#endif

BOOLEAN NxpNci_Connect(void)
{
    uint8_t i = 10;
    uint8_t NCICoreInit[] = {0x20, 0x01, 0x00};
    uint8_t Answer[256];
    uint16_t AnswerSize;

    /* Open connection to NXPNCI */
    tml_Connect();

    /* Loop until NXPNCI answers */
    while(NxpNci_CheckDevPres() != NXPNCI_SUCCESS)
        {
            if(i-- == 0) return NXPNCI_ERROR;
            delay_ms(500);
        }

    NxpNci_HostTransceive(NCICoreInit, sizeof(NCICoreInit), Answer, sizeof(Answer), &AnswerSize);
    if ((Answer[0] != 0x40) || (Answer[1] != 0x01) || (Answer[3] != 0x00)) return NXPNCI_ERROR;

    return NXPNCI_SUCCESS;
}

BOOLEAN NxpNci_Configure(unsigned char mode)
{
    uint8_t Command[256];
    uint8_t Answer[256];
    uint16_t AnswerSize;
    uint8_t Item = 0;

    uint8_t NCIDiscoverMap[] = {0x21, 0x00};
#ifdef CARDEMU_SUPPORT
    const uint8_t DM_CARDEMU[] = {0x4, 0x2, 0x2};
    const uint8_t R_CARDEMU[] = {0x1, 0x3, 0x0, 0x1, 0x4};
#endif
#ifdef P2P_SUPPORT
    const uint8_t DM_P2P[] = {0x5, 0x3, 0x3};
    const uint8_t R_P2P[] = {0x1, 0x3, 0x0, 0x1, 0x5};
    uint8_t NCISetConfig_NFC[] = {0x20, 0x02, 0x1F, 0x02, 0x29, 0x0D, 0x46, 0x66, 0x6D, 0x01, 0x01, 0x11, 0x03, 0x02, 0x00, 0x01, 0x04, 0x01, 0xFA, 0x61, 0x0D, 0x46, 0x66, 0x6D, 0x01, 0x01, 0x11, 0x03, 0x02, 0x00, 0x01, 0x04, 0x01, 0xFA};
#endif
#ifdef RW_SUPPORT
    const uint8_t DM_RW[] = {0x1, 0x1, 0x1,	 0x2, 0x1, 0x1,  0x3, 0x1, 0x1,  0x4, 0x1, 0x2,  0x80, 0x01, 0x80};
    uint8_t NCIPropAct[] = {0x2F, 0x02, 0x00};
#endif
#if defined P2P_SUPPORT || defined CARDEMU_SUPPORT
    uint8_t NCIRouting[] = {0x21, 0x01, 0x07, 0x00, 0x01};
    uint8_t NCISetConfig_NFCA_SELRSP[] = {0x20, 0x02, 0x04, 0x01, 0x32, 0x01, 0x00};
#endif

    if(mode == 0) return NXPNCI_SUCCESS;

    /* Enable Proprietary interface for T4T card presence check procedure */
#ifdef RW_SUPPORT
    if (mode == NXPNCI_MODE_RW)
        {
            NxpNci_HostTransceive(NCIPropAct, sizeof(NCIPropAct), Answer, sizeof(Answer), &AnswerSize);
            if ((Answer[0] != 0x4F) || (Answer[1] != 0x02) || (Answer[3] != 0x00)) return NXPNCI_ERROR;
        }
#endif

    /* Building Discovery Map command */
    Item = 0;
#ifdef CARDEMU_SUPPORT
    if (mode & NXPNCI_MODE_CARDEMU)
        {
            memcpy(&Command[4+(3*Item)], DM_CARDEMU, sizeof(DM_CARDEMU));
            Item++;
        }
#endif
#ifdef P2P_SUPPORT
    if (mode & NXPNCI_MODE_P2P)
        {
            memcpy(&Command[4+(3*Item)], DM_P2P, sizeof(DM_P2P));
            Item++;
        }
#endif
#ifdef RW_SUPPORT
    if (mode & NXPNCI_MODE_RW)
        {
            memcpy(&Command[4+(3*Item)], DM_RW, sizeof(DM_RW));
            Item+=sizeof(DM_RW)/3;
        }
#endif
#if defined P2P_SUPPORT || defined CARDEMU_SUPPORT || defined RW_SUPPORT
    if (Item != 0)
        {
            memcpy(Command, NCIDiscoverMap, sizeof(NCIDiscoverMap));
            Command[2] = 1 + (Item*3);
            Command[3] = Item;
            NxpNci_HostTransceive(Command, 3 + Command[2], Answer, sizeof(Answer), &AnswerSize);
            if ((Answer[0] != 0x41) || (Answer[1] != 0x00) || (Answer[3] != 0x00)) return NXPNCI_ERROR;
        }
#endif

    /* Configuring routing */
    Item = 0;
#ifdef CARDEMU_SUPPORT
    if (mode & NXPNCI_MODE_CARDEMU)
        {
            memcpy(&Command[5+(5*Item)], R_CARDEMU, sizeof(R_CARDEMU));
            Item++;
        }
#endif
#ifdef P2P_SUPPORT
    if (mode & NXPNCI_MODE_P2P)
        {
            memcpy(&Command[5+(5*Item)], R_P2P, sizeof(R_P2P));
            Item++;
        }
#endif
#if defined P2P_SUPPORT || defined CARDEMU_SUPPORT
    if (Item != 0)
        {
            memcpy(Command, NCIRouting, sizeof(NCIRouting));
            Command[2] = 2 + (Item*5);
            Command[4] = Item;
            NxpNci_HostTransceive(Command, 3 + Command[2] , Answer, sizeof(Answer), &AnswerSize);
            if ((Answer[0] != 0x41) || (Answer[1] != 0x01) || (Answer[3] != 0x00)) return NXPNCI_ERROR;
        }
#endif

    /* Setting NFCA SEL_RSP */
#ifdef CARDEMU_SUPPORT
    if (mode & NXPNCI_MODE_CARDEMU)
        {
            NCISetConfig_NFCA_SELRSP[6] += 0x20;
        }
#endif
#ifdef P2P_SUPPORT
    if (mode & NXPNCI_MODE_P2P)
        {
            NCISetConfig_NFCA_SELRSP[6] += 0x40;
        }
#endif
#if defined P2P_SUPPORT || defined CARDEMU_SUPPORT
    if (NCISetConfig_NFCA_SELRSP[6] != 0x00)
        {
            NxpNci_HostTransceive(NCISetConfig_NFCA_SELRSP, sizeof(NCISetConfig_NFCA_SELRSP), Answer, sizeof(Answer), &AnswerSize);
            if ((Answer[0] != 0x40) || (Answer[1] != 0x02) || (Answer[3] != 0x00)) return NXPNCI_ERROR;
        }
#endif

    /* Setting LLCP support */
#ifdef P2P_SUPPORT
    if (mode & NXPNCI_MODE_P2P)
        {
            NxpNci_HostTransceive(NCISetConfig_NFC, sizeof(NCISetConfig_NFC), Answer, sizeof(Answer), &AnswerSize);
            if ((Answer[0] != 0x40) || (Answer[1] != 0x02) || (Answer[3] != 0x00)) return NXPNCI_ERROR;
        }
#endif

#warning Inserted Setting!
    uint8_t NxpNci_CORE_CONF[]= {0x20, 0x02, 0x05, 0x01,        /* CORE_SET_CONFIG_CMD */
                                 0x00, 0x02, 0x00, 0x01                                  /* TOTAL_DURATION */
                                };
								
    NxpNci_HostTransceive(NxpNci_CORE_CONF, sizeof(NxpNci_CORE_CONF), Answer, sizeof(Answer), &AnswerSize);
	
    uint8_t NCICoreReset[] = {0x20, 0x00, 0x01, 0x00};
    uint8_t NCICoreInit[] = {0x20, 0x01, 0x00};
		
    NxpNci_HostTransceive(NCICoreReset, sizeof(NCICoreReset), Answer, sizeof(Answer), &AnswerSize);
    if ((Answer[0] != 0x40) || (Answer[1] != 0x00) || (Answer[3] != 0x00)) return NXPNCI_ERROR;
	
    NxpNci_HostTransceive(NCICoreInit, sizeof(NCICoreInit), Answer, sizeof(Answer), &AnswerSize);
    if ((Answer[0] != 0x40) || (Answer[1] != 0x01) || (Answer[3] != 0x00)) return NXPNCI_ERROR;


    return NXPNCI_SUCCESS;
}

BOOLEAN NxpNci_StartDiscovery(unsigned char *pTechTab, unsigned char TechTabSize)
{
    uint8_t Buffer[256];
    uint8_t Answer[256];
    uint16_t AnswerSize;
    uint8_t i;

    Buffer[0] = 0x21;
    Buffer[1] = 0x03;
    Buffer[2] = (TechTabSize * 2) + 1;
    Buffer[3] = TechTabSize;
    for (i=0; i<TechTabSize; i++)
        {
            Buffer[(i*2)+4] = pTechTab[i];
            Buffer[(i*2)+5] = 0x01;
        }

    NxpNci_HostTransceive(Buffer, (TechTabSize * 2) + 4, Answer, sizeof(Answer), &AnswerSize);
    if ((Answer[0] != 0x41) || (Answer[1] != 0x03) || (Answer[3] != 0x00)) return NXPNCI_ERROR;

    return NXPNCI_SUCCESS;
}

BOOLEAN NxpNci_RestartDiscovery(void)
{
    uint8_t NCIRestartDiscovery[] = {0x21, 0x06, 0x01, 0x03};
    uint8_t Answer[256];
    uint16_t AnswerSize;

    NxpNci_HostTransceive(NCIRestartDiscovery, sizeof(NCIRestartDiscovery), Answer, sizeof(Answer), &AnswerSize);
    NxpNci_WaitForReception(Answer, sizeof(Answer), &AnswerSize, TIMEOUT_100MS);

    return NXPNCI_SUCCESS;
}

BOOLEAN NxpNci_StopDiscovery(void)
{
    uint8_t NCIRestartDiscovery[] = {0x21, 0x06, 0x01, 0x00};
    uint8_t Answer[256];
    uint16_t AnswerSize;


    while(NxpNci_HostTransceive(NCIRestartDiscovery, sizeof(NCIRestartDiscovery), Answer, sizeof(Answer), &AnswerSize) != NXPNCI_SUCCESS)
        {
            //delay_ms(100);
        }
    NxpNci_WaitForReception(Answer, sizeof(Answer), &AnswerSize, TIMEOUT_100MS);

    return NXPNCI_SUCCESS;
}

void NxpNci_WaitForDiscoveryNotification(NxpNci_RfIntf_t *pRfIntf)
{
    uint8_t AnswerBuffer[256];
    uint16_t AnswerSize;

    do
        {
            NxpNci_WaitForReception(AnswerBuffer, sizeof(AnswerBuffer), &AnswerSize, TIMEOUT_INFINITE);
        }
    while ((AnswerBuffer[0] != 0x61) || ((AnswerBuffer[1] != 0x05) && (AnswerBuffer[1] != 0x03)));

    /* Is RF_INTF_ACTIVATED_NTF ? */
    if (AnswerBuffer[1] == 0x05)
        {
            pRfIntf->Interface = AnswerBuffer[4];
            pRfIntf->Protocol = AnswerBuffer[5];
            pRfIntf->ModeTech = AnswerBuffer[6];
        }
    else /* RF_DISCOVER_NTF */
        {
            pRfIntf->Interface = INTF_UNDETERMINED;
            pRfIntf->Protocol = AnswerBuffer[4];
            pRfIntf->ModeTech = AnswerBuffer[5];
            /* Remaining NTF ? */
            while(AnswerBuffer[AnswerSize-1] == 0x02)	NxpNci_WaitForReception(AnswerBuffer, sizeof(AnswerBuffer), &AnswerSize, TIMEOUT_1S);
        }
}


BOOLEAN NxpNci_PollDiscoveryNotification(NxpNci_RfIntf_t *pRfIntf)
{



    uint8_t AnswerBuffer[256];
    uint16_t AnswerSize;
    BOOLEAN NCI_result;
    uint8_t valid = 0;

    NCI_result = NxpNci_WaitForReception(AnswerBuffer, sizeof(AnswerBuffer), &AnswerSize, TIMEOUT_100MS);

    if((NCI_result == NXPNCI_SUCCESS) && (AnswerBuffer[0] == 0x61) && ((AnswerBuffer[1] == 0x05) || (AnswerBuffer[1] == 0x03)))
        {


            /* Is RF_INTF_ACTIVATED_NTF ? */
            if (AnswerBuffer[1] == 0x05)
                {
                    pRfIntf->Interface = AnswerBuffer[4];
                    pRfIntf->Protocol = AnswerBuffer[5];
                    pRfIntf->ModeTech = AnswerBuffer[6];
                }
            else /* RF_DISCOVER_NTF */
                {
                    pRfIntf->Interface = INTF_UNDETERMINED;
                    pRfIntf->Protocol = AnswerBuffer[4];
                    pRfIntf->ModeTech = AnswerBuffer[5];
                    /* Remaining NTF ? */
                    while(AnswerBuffer[AnswerSize-1] == 0x02)	NxpNci_WaitForReception(AnswerBuffer, sizeof(AnswerBuffer), &AnswerSize, TIMEOUT_100MS);
                }
            return TRUE;
        }
    else
        {
            gpio_set_pin_level(LEDYEL, LOW);
            //gpio_set_pin_level(LEDGRN, LOW);
            return FALSE;
        }
}


void NxpNci_Process(unsigned char mode, NxpNci_RfIntf_t RfIntf)
{
    switch (mode)
        {
#ifdef CARDEMU_SUPPORT
        case NXPNCI_MODE_CARDEMU:
            NxpNci_CardMode(RfIntf);
            break;
#endif

#ifdef P2P_SUPPORT
        case NXPNCI_MODE_P2P:
            NxpNci_P2pMode(RfIntf);
            break;
#endif

#ifdef RW_SUPPORT
        case NXPNCI_MODE_RW:
            NxpNci_ReaderMode(RfIntf);
            break;
#endif

        default:
            break;
        }
}

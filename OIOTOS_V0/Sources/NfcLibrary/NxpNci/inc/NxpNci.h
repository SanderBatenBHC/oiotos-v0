#include <types.h>


#define NXPNCI_SUCCESS	0
#define NXPNCI_ERROR	1

#define NXPNCI_MODE_CARDEMU	0x01
#define NXPNCI_MODE_P2P		0x02
#define NXPNCI_MODE_RW		0x04

#define INTF_UNDETERMINED	0x0
#define INTF_FRAME			0x1
#define INTF_ISODEP			0x2
#define INTF_NFCDEP			0x3

#define PROT_UNDETERMINED	0x0
#define PROT_T1T			0x1
#define PROT_T2T			0x2
#define PROT_T3T			0x3
#define PROT_ISODEP			0x4
#define PROT_NFCDEP			0x5
#define PROT_MIFARE			0x80

#define MODE_POLL			0x00
#define MODE_LISTEN			0x80
#define MODE_MASK			0xF0

#define TECH_PASSIVE_NFCA	0
#define TECH_PASSIVE_NFCB	1
#define TECH_PASSIVE_NFCF	2
#define TECH_ACTIVE_NFCA	3
#define TECH_ACTIVE_NFCF	5
#define TECH_PASSIVE_15693	6

typedef struct
{
	unsigned char Interface;
	unsigned char Protocol;
	unsigned char ModeTech;
} NxpNci_RfIntf_t;

extern BOOLEAN NxpNci_Connect(void);
extern BOOLEAN NxpNci_Configure(unsigned char mode);
extern BOOLEAN NxpNci_StartDiscovery(unsigned char *pTechTab, unsigned char TechTabSize);
extern void NxpNci_WaitForDiscoveryNotification(NxpNci_RfIntf_t *pRfIntf);
extern BOOLEAN NxpNci_PollDiscoveryNotification(NxpNci_RfIntf_t *pRfIntf);
extern void NxpNci_Process(unsigned char mode, NxpNci_RfIntf_t RfIntf);
extern BOOLEAN NxpNci_RestartDiscovery(void);
extern BOOLEAN NxpNci_StopDiscovery(void);

#ifdef RW_SUPPORT
extern BOOLEAN NxpNci_ReaderTagCmd (unsigned char *Command, unsigned char CommandSize, unsigned char *Answer, unsigned char *AnswerSize);
#endif

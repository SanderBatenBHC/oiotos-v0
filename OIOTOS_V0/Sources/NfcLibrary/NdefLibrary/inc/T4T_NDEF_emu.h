#include <types.h>

extern void T4T_NDEF_EMU_Reset(void);
extern void T4T_NDEF_EMU_Next(unsigned char *pCmd, unsigned short Cmd_size, unsigned char *Rsp, unsigned short *pRsp_size);
extern BOOLEAN T4T_NDEF_EMU_SetRecord(unsigned char *pRecord, unsigned short Record_size, void *cb);

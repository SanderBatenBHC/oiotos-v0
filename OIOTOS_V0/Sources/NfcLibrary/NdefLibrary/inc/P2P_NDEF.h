#include <types.h>

#define P2P_NDEF_MAX_NDEF_RECORD_SIZE 240

extern void P2P_NDEF_Reset(void);
extern void P2P_NDEF_Next(unsigned char *pCmd, unsigned short Cmd_size, unsigned char *Rsp, unsigned short *pRsp_size);
extern BOOLEAN P2P_NDEF_SetRecord(unsigned char *pRecord, unsigned short Record_size, void *cb);
extern void P2P_NDEF_RegisterPullCallback(void *pCb);

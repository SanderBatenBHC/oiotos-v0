#include <types.h>

#define RW_MAX_NDEF_FILE_SIZE	850
#define TAG_UID_SIZE	7

extern unsigned char NdefBuffer[RW_MAX_NDEF_FILE_SIZE];
extern unsigned char NdefTagUID[TAG_UID_SIZE];

typedef void RW_NDEF_Callback_t (unsigned char*, unsigned short);

#define RW_NDEF_TYPE_T1T	0x1
#define RW_NDEF_TYPE_T2T	0x2
#define RW_NDEF_TYPE_T3T	0x3
#define RW_NDEF_TYPE_T4T	0x4


#define NDEF_RECORD_MB_MASK		0x80
#define NDEF_RECORD_ME_MASK		0x40
#define NDEF_RECORD_CF_MASK		0x20
#define NDEF_RECORD_SR_MASK		0x10
#define NDEF_RECORD_IL_MASK		0x08
#define NDEF_RECORD_TNF_MASK	0x07


#define NDEF_EMPTY			0x00
#define NDEF_WELL_KNOWN		0x01
#define NDEF_MEDIA			0x02
#define NDEF_ABSOLUTE_URI	0x03
#define NDEF_EXTERNAL		0x04
#define NDEF_UNKNOWN		0x05
#define NDEF_UNCHANGED		0x06
#define NDEF_RESERVED		0x07


RW_NDEF_Callback_t *pRW_NDEF_PullCb;
extern void RW_NDEF_RegisterPullCallback(void *pCb);
extern void RW_NDEF_Reset(unsigned char type);
extern void RW_NDEF_Read_Next(unsigned char *pCmd, unsigned short Cmd_size, unsigned char *Rsp, unsigned short *pRsp_size);

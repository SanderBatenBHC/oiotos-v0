#ifdef RW_SUPPORT
#include <RW_NDEF.h>
#include <RW_NDEF_T1T.h>
#include <RW_NDEF_T2T.h>
#include <RW_NDEF_T3T.h>
#include <RW_NDEF_T4T.h>

typedef void RW_NDEF_ReadFct_t (unsigned char *pCmd, unsigned short Cmd_size, unsigned char *Rsp, unsigned short *pRsp_size);

/* Allocate buffer for NDEF operations */
unsigned char NdefBuffer[RW_MAX_NDEF_FILE_SIZE];
unsigned char NdefTagUID[TAG_UID_SIZE];

static RW_NDEF_ReadFct_t *pReadFct = NULL;

void RW_NDEF_RegisterPullCallback(void *pCb)
{
	pRW_NDEF_PullCb = (RW_NDEF_Callback_t *) pCb;
}

void RW_NDEF_Reset(unsigned char type)
{
	switch (type)
	{
	case RW_NDEF_TYPE_T1T:
		RW_NDEF_T1T_Reset();
		pReadFct = RW_NDEF_T1T_Read_Next;
		break;
	case RW_NDEF_TYPE_T2T:
		RW_NDEF_T2T_Reset();
		pReadFct = RW_NDEF_T2T_Read_Next;
		break;
	case RW_NDEF_TYPE_T3T:
		RW_NDEF_T3T_Reset();
		pReadFct = RW_NDEF_T3T_Read_Next;
		break;
	case RW_NDEF_TYPE_T4T:
		RW_NDEF_T4T_Reset();
		pReadFct = RW_NDEF_T4T_Read_Next;
		break;
	default:
		pReadFct = NULL;
		break;
	}
}

void RW_NDEF_Read_Next(unsigned char *pCmd, unsigned short Cmd_size, unsigned char *Rsp, unsigned short *pRsp_size)
{
	if (pReadFct != NULL) pReadFct(pCmd, Cmd_size, Rsp, pRsp_size);
}
#endif

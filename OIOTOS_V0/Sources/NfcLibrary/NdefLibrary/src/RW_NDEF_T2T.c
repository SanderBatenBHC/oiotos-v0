#ifdef RW_SUPPORT
#include <string.h>
#include <RW_NDEF.h>

/* TODO: No support for tag larger than 1024 bytes (requiring SECTOR_SELECT command use) */

#define T2T_MAGIC_NUMBER	0xE1
#define T2T_NDEF_TLV		0x03

typedef enum
{
	Initial,
	Reading_UID,
	Reading_CC,
	Reading_Data,
	Reading_NDEF
} RW_NDEF_T2T_state_t;

typedef struct
{
	unsigned char BlkNb;
	unsigned short RecordPtr;
	unsigned short RecordSize;
	unsigned char *pRecord;
} RW_NDEF_T2T_Ndef_t;

static RW_NDEF_T2T_state_t eRW_NDEF_T2T_State = Initial;
static RW_NDEF_T2T_Ndef_t RW_NDEF_T2T_Ndef;

void RW_NDEF_T2T_Reset(void)
{
	eRW_NDEF_T2T_State = Initial;
	RW_NDEF_T2T_Ndef.pRecord = NdefBuffer;
}

void RW_NDEF_T2T_Read_Next(unsigned char *pRsp, unsigned short Rsp_size, unsigned char *pCmd, unsigned short *pCmd_size)
{
	/* By default no further command to be sent */
	*pCmd_size = 0;

	switch(eRW_NDEF_T2T_State)
	{
	case Initial:
		/* Read UID */
		pCmd[0] = 0x30;
		pCmd[1] = 0x00;
		*pCmd_size = 2;
		eRW_NDEF_T2T_State = Reading_UID;
		break;

	case Reading_UID:
		/* Is Read success ?*/
		if ((Rsp_size == 17) && (pRsp[Rsp_size-1] == 0x00))
		{
			/* Retrieve UID */
			NdefTagUID[0] = pRsp[0];
			NdefTagUID[1] = pRsp[1];
			NdefTagUID[2] = pRsp[2];
			NdefTagUID[3] = pRsp[4];
			NdefTagUID[4] = pRsp[5];
			NdefTagUID[5] = pRsp[6];
			NdefTagUID[6] = pRsp[7];

			/* Read CC */
			pCmd[0] = 0x30;
			pCmd[1] = 0x03;
			*pCmd_size = 2;

			eRW_NDEF_T2T_State = Reading_CC;
		}
		break;

	case Reading_CC:
		/* Is CC Read and Is Ndef ?*/
		if ((Rsp_size == 17) && (pRsp[Rsp_size-1] == 0x00) && (pRsp[0] == T2T_MAGIC_NUMBER))
		{
			/* Read First data */
			pCmd[0] = 0x30;
			pCmd[1] = 0x04;
			*pCmd_size = 2;

			eRW_NDEF_T2T_State = Reading_Data;
		}
		break;

	case Reading_Data:
		/* Is Read success ?*/
		if ((Rsp_size == 17) && (pRsp[Rsp_size-1] == 0x00))
		{
			unsigned char Tmp = 0;
			/* If not NDEF Type skip TLV */
			while (pRsp[Tmp] != T2T_NDEF_TLV)
			{
				Tmp += 2 + pRsp[Tmp+1];
				if (Tmp > Rsp_size) return;
			}

			if(pRsp[Tmp+1] == 0xFF) RW_NDEF_T2T_Ndef.RecordSize = (pRsp[Tmp+2] << 8) + pRsp[Tmp+3];
			else RW_NDEF_T2T_Ndef.RecordSize = pRsp[Tmp+1];

			if(pRsp[Tmp+1] == 0xFF)
			{
				RW_NDEF_T2T_Ndef.RecordPtr = (Rsp_size-1) - Tmp - 4;
				memcpy (RW_NDEF_T2T_Ndef.pRecord, &pRsp[Tmp+4], RW_NDEF_T2T_Ndef.RecordPtr);
				RW_NDEF_T2T_Ndef.BlkNb = 8;

				/* Read NDEF content */
				pCmd[0] = 0x30;
				pCmd[1] = RW_NDEF_T2T_Ndef.BlkNb;
				*pCmd_size = 2;
				eRW_NDEF_T2T_State = Reading_NDEF;
			}
			else
			{
				/* Is NDEF read already completed ? */
				if (RW_NDEF_T2T_Ndef.RecordSize <= ((Rsp_size-1) - Tmp - 2))
				{
					memcpy (RW_NDEF_T2T_Ndef.pRecord, &pRsp[Tmp+2], RW_NDEF_T2T_Ndef.RecordSize);

					/* Notify application of the NDEF reception */
					if(pRW_NDEF_PullCb != NULL) pRW_NDEF_PullCb(RW_NDEF_T2T_Ndef.pRecord, RW_NDEF_T2T_Ndef.RecordSize);
				}
				else
				{
					RW_NDEF_T2T_Ndef.RecordPtr = (Rsp_size-1) - Tmp - 2;
					memcpy (RW_NDEF_T2T_Ndef.pRecord, &pRsp[Tmp+2], RW_NDEF_T2T_Ndef.RecordPtr);
					RW_NDEF_T2T_Ndef.BlkNb = 8;

					/* Read NDEF content */
					pCmd[0] = 0x30;
					pCmd[1] = RW_NDEF_T2T_Ndef.BlkNb;
					*pCmd_size = 2;
					eRW_NDEF_T2T_State = Reading_NDEF;
				}
			}
		}
		break;

		case Reading_NDEF:
		/* Is Read success ?*/
		if ((Rsp_size == 17) && (pRsp[Rsp_size-1] == 0x00))
		{
			/* Is NDEF read already completed ? */
			if ((RW_NDEF_T2T_Ndef.RecordSize - RW_NDEF_T2T_Ndef.RecordPtr) < 16)
			{
				memcpy (&RW_NDEF_T2T_Ndef.pRecord[RW_NDEF_T2T_Ndef.RecordPtr], pRsp, RW_NDEF_T2T_Ndef.RecordSize - RW_NDEF_T2T_Ndef.RecordPtr);

				/* Notify application of the NDEF reception */
				if(pRW_NDEF_PullCb != NULL) pRW_NDEF_PullCb(RW_NDEF_T2T_Ndef.pRecord, RW_NDEF_T2T_Ndef.RecordSize);
			}
			else
			{
				memcpy (&RW_NDEF_T2T_Ndef.pRecord[RW_NDEF_T2T_Ndef.RecordPtr], pRsp, 16);
				RW_NDEF_T2T_Ndef.RecordPtr += 16;
				RW_NDEF_T2T_Ndef.BlkNb += 4;

				/* Read NDEF content */
				pCmd[0] = 0x30;
				pCmd[1] = RW_NDEF_T2T_Ndef.BlkNb;
				*pCmd_size = 2;
			}
		}
		break;

	default:
		break;
	}
}
#endif

#ifdef P2P_SUPPORT
#include <string.h>
#include <P2P_NDEF.h>



/* Well-known LLCP SAP Values */
#define SAP_SDP			1
#define SAP_SNEP		4

/* SNEP codes */
#define SNEP_VER10		0x10
#define SNEP_PUT		0x2
#define SNEP_SUCCESS	0x81

/* LLCP PDU Types */
#define SYMM			0x0
#define PAX				0x1
#define AGF				0x2
#define UI				0x3
#define CONNECT			0x4
#define DISC			0x5
#define CC				0x6
#define DM				0x7
#define FRMR			0x8
#define SNL				0x9
#define reservedA		0xA
#define reservedB		0xB
#define I				0xC
#define RR				0xD
#define RNR				0xE
#define reservedF		0xF

const unsigned char SNEP_PUT_SUCCESS[] = {SNEP_VER10, SNEP_SUCCESS, 0x00, 0x00, 0x00, 0x00};
const unsigned char LLCP_CONNECT_SNEP[] = {0x11, 0x20};
const unsigned char LLCP_I_SNEP_PUT_HEADER[] = {SNEP_VER10, SNEP_PUT, 0x00, 0x00, 0x00, 0x00};
const unsigned char LLCP_SYMM[] = {0x00, 0x00};

unsigned char *pNdefRecord;
unsigned short NdefRecord_size = 0;

/* Defines the number of symmetry exchanges is expected before initiating the NDEF push (to allow a remote phone to beam an NDEF message first) */
#define NDEF_PUSH_DELAY_COUNT	1

/* Defines at which frequency the symmetry is exchange (in ms) */
#define SYMM_FREQ	500

typedef enum
{
	Idle,
    Initial,
	DelayingPush,
	InitiatePush,
	SnepClientConnecting,
    SnepClientConnected,
	NdefMsgSent
} P2P_SnepClient_state_t;

typedef struct
{
    unsigned char Dsap;
    unsigned char Pdu;
    unsigned char Ssap;
} P2P_NDEF_LlcpHeader_t;

typedef void P2P_NDEF_Callback_t (unsigned char*, unsigned short);

static P2P_SnepClient_state_t eP2P_SnepClient_State = Initial;
static P2P_NDEF_Callback_t *pP2P_NDEF_PushCb = NULL;
static P2P_NDEF_Callback_t *pP2P_NDEF_PullCb = NULL;
static unsigned short P2P_SnepClient_DelayCount = NDEF_PUSH_DELAY_COUNT;

static void ParseLlcp(unsigned char *pBuf, P2P_NDEF_LlcpHeader_t *pLlcpHeader)
{
	pLlcpHeader->Dsap = pBuf[0] >> 2;
	pLlcpHeader->Pdu = ((pBuf[0] & 3) << 2) + (pBuf[1] >> 6);
	pLlcpHeader->Ssap = pBuf[1] & 0x3F;
}

static void FillLlcp(P2P_NDEF_LlcpHeader_t LlcpHeader, unsigned char *pBuf)
{
	pBuf[0] = (LlcpHeader.Ssap << 2) + ((LlcpHeader.Pdu >> 2) & 3);
	pBuf[1] = (LlcpHeader.Pdu << 6) + LlcpHeader.Dsap;
}

BOOLEAN P2P_NDEF_SetRecord(unsigned char *pRecord, unsigned short Record_size, void *cb)
{
    if (Record_size <= P2P_NDEF_MAX_NDEF_RECORD_SIZE)
    {
        pNdefRecord = pRecord;
        NdefRecord_size = Record_size;
        pP2P_NDEF_PushCb = (P2P_NDEF_Callback_t*) cb;
		return TRUE;
    }
    else
    {
        NdefRecord_size = 0;
		pP2P_NDEF_PushCb = NULL;
		return FALSE;
    }
}

void P2P_NDEF_RegisterPullCallback(void *pCb)
{
	pP2P_NDEF_PullCb = (P2P_NDEF_Callback_t*) pCb;
}

void P2P_NDEF_Reset(void)
{
	if (NdefRecord_size != 0)
	{
		eP2P_SnepClient_State = Initial;
	}
	else
	{
		eP2P_SnepClient_State = Idle;
	}
}

void P2P_NDEF_Next(unsigned char *pCmd, unsigned short Cmd_size, unsigned char *pRsp, unsigned short *pRsp_size)
{
	P2P_NDEF_LlcpHeader_t LlcpHeader;
	
	/* Initialize answer */
	*pRsp_size = 0;

	ParseLlcp(pCmd, &LlcpHeader);
	
	switch (LlcpHeader.Pdu)
	{
	case CONNECT:
		/* Is connection from SNEP Client ? */
		if (LlcpHeader.Dsap == SAP_SNEP)
		{
			/* Only accept the connection is application is registered for NDEF reception */
			if(pP2P_NDEF_PullCb != NULL)
			{
				LlcpHeader.Pdu = CC;
				FillLlcp(LlcpHeader, pRsp);
				*pRsp_size = 2;
			}
		}
		else
		{
			/* Refuse any other connection request */
			LlcpHeader.Pdu = DM;
			FillLlcp(LlcpHeader, pRsp);
			*pRsp_size = 2;
		}
		break;

	case I:
		/* Is SNEP PUT ? */
		if ((pCmd[3] == SNEP_VER10) && (pCmd[4] == SNEP_PUT))
		{
			/* Notify application of the NDEF reception */
			if(pP2P_NDEF_PullCb != NULL) pP2P_NDEF_PullCb(&pCmd[9], pCmd[8]);

			/* Acknowledge the PUT request */
			LlcpHeader.Pdu = I;
			FillLlcp(LlcpHeader, pRsp);
			pRsp[2] = (pCmd[2] >> 4) + 1; // N(R)
			memcpy(&pRsp[3], SNEP_PUT_SUCCESS, sizeof(SNEP_PUT_SUCCESS));
			*pRsp_size = 9;
		}
		break;

	case CC:
		/* Connection to remote SNEP server completed, send NDEF record inside SNEP PUT request */
		eP2P_SnepClient_State = SnepClientConnected;
		break;

	default:
		break;

	}

	/* No anwsers was set */
	if (*pRsp_size == 0)
	{
		switch(eP2P_SnepClient_State)
		{
		case InitiatePush:
			memcpy(pRsp, LLCP_CONNECT_SNEP, sizeof(LLCP_CONNECT_SNEP));
			*pRsp_size = sizeof(LLCP_CONNECT_SNEP);
			eP2P_SnepClient_State = SnepClientConnecting;
			break;

		case SnepClientConnected:
			LlcpHeader.Pdu = I;
			FillLlcp(LlcpHeader, pRsp);
			pRsp[2] = 0; // N(R)
			pRsp[3] = SNEP_VER10;
			pRsp[4] = SNEP_PUT;
			pRsp[5] = 0;
			pRsp[6] = 0;
			pRsp[7] = 0;
			pRsp[8] = NdefRecord_size;
			memcpy(&pRsp[9], pNdefRecord, NdefRecord_size);
			*pRsp_size = 9 + NdefRecord_size;
			eP2P_SnepClient_State = NdefMsgSent;
			/* Notify application of the NDEF push */
			if(pP2P_NDEF_PushCb != NULL) pP2P_NDEF_PushCb(pNdefRecord, NdefRecord_size);
			break;

		case DelayingPush:
			P2P_SnepClient_DelayCount++;
			if(P2P_SnepClient_DelayCount == NDEF_PUSH_DELAY_COUNT)
			{
				eP2P_SnepClient_State = InitiatePush;
			}
			/* send a SYMM */
			delay_ms(1000);
			memcpy(pRsp, LLCP_SYMM, sizeof(LLCP_SYMM));
			*pRsp_size = sizeof(LLCP_SYMM);
			break;

		case Initial:
			P2P_SnepClient_DelayCount = 0;
			eP2P_SnepClient_State = DelayingPush;
		default:
			/* send a SYMM */
			delay_ms(SYMM_FREQ);
			memcpy(pRsp, LLCP_SYMM, sizeof(LLCP_SYMM));
			*pRsp_size = sizeof(LLCP_SYMM);
			break;
		}
	}
}
#endif

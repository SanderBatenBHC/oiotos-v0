#include <stdint.h>
#include <types.h>

#include "tml.h"
#include <driver_init.h>


static uint8_t tml_Init(void) {
// 	I2C_DRV_MasterInit(NFCC_I2C_INSTANCE, &master);
// 	GPIO_DRV_InputPinInit(&NFCCirqPin);
// 	GPIO_DRV_OutputPinInit(&NFCCvenPin);

	return SUCCESS;
}

static uint8_t tml_Reset(void) {
	/* Apply VEN reset */
	gpio_set_pin_level(RFID_VEN, HIGH);
	delay_ms(10);
	gpio_set_pin_level(RFID_VEN, LOW);
	delay_ms(10);
	gpio_set_pin_level(RFID_VEN, HIGH);

	return SUCCESS;
}

static uint8_t tml_Tx(uint8_t *pBuff, uint16_t buffLen, uint16_t *pBytesWritten) {
	uint8_t ret;
	uint8_t write;
struct io_descriptor *I2C_0_io;

i2c_m_sync_get_io_descriptor(&I2C_0, &I2C_0_io);
i2c_m_sync_enable(&I2C_0);
i2c_m_sync_set_slaveaddr(&I2C_0, NXPNCI_I2C_ADDR, I2C_M_SEVEN);
write = io_write(I2C_0_io, pBuff, buffLen);
if (write > 0) ret = SUCCESS;

// 	ret = I2C_DRV_MasterSendDataBlocking(NFCC_I2C_INSTANCE, &device, NULL, 0, (const uint8_t*)pBuff, (uint32_t)buffLen, 1000);

	if (ret == SUCCESS) {
		*pBytesWritten = buffLen;
	} else {
		*pBytesWritten = 0;
	}

	return ret;
}

static uint8_t tml_Rx(uint8_t *pBuff, uint16_t buffLen, uint16_t *pBytesRead) {
	uint8_t ret;
	uint8_t read;


struct io_descriptor *I2C_0_io;
i2c_m_sync_get_io_descriptor(&I2C_0, &I2C_0_io);
i2c_m_sync_enable(&I2C_0);
i2c_m_sync_set_slaveaddr(&I2C_0, NXPNCI_I2C_ADDR, I2C_M_SEVEN);
read = io_read(I2C_0_io,pBuff,3);
if (read > 0) ret = SUCCESS;

// 	ret = I2C_DRV_MasterReceiveDataBlocking(NFCC_I2C_INSTANCE, &device, NULL, 0, pBuff, 3, 1000);
// 
 	if (ret == SUCCESS) {
 		if (pBuff[2] != 0) {
// 			ret = I2C_DRV_MasterReceiveDataBlocking(NFCC_I2C_INSTANCE, &device, NULL, 0, &pBuff[3], (uint32_t)pBuff[2], 1000);
			read = io_read(I2C_0_io,&pBuff[3], (uint32_t)pBuff[2]) ;
			if (read > 0) ret = SUCCESS;
 			if (ret == SUCCESS) {
 				*pBytesRead = pBuff[2] + 3;
			} else {
 				*pBytesRead = 0;
 			}
 		} else {
 			*pBytesRead = 3;
 		}
 	} else {
 		*pBytesRead = 0;
 	}

	return ret;

}

static uint8_t tml_WaitForRx(uint16_t timeout) {
	uint32_t loop = 0;

	if (timeout == 0) {
		while ( gpio_get_pin_level(RFID_IRQ) == LOW)
			;
	} else {
		while ( gpio_get_pin_level(RFID_IRQ) == LOW) {
			if ((loop++ > (timeout * 350)))
				return ERROR;
		};
	}
	return SUCCESS;
}

void tml_Connect(void) {
	tml_Init();
	tml_Reset();
}

void tml_Send(uint8_t *pBuffer, uint16_t BufferLen, uint16_t *pBytesSent) {
	tml_Tx(pBuffer, BufferLen, pBytesSent);
	delay_ms(10);
}

void tml_Receive(uint8_t *pBuffer, uint16_t BufferLen, uint16_t *pBytes,
		uint16_t timeout) {
	if (tml_WaitForRx(timeout) == ERROR)
		*pBytes = 0;
	else
		tml_Rx(pBuffer, BufferLen, pBytes);
}


#define TIMEOUT_INFINITE	0
#define TIMEOUT_100MS		100
#define TIMEOUT_1S			1000
#define TIMEOUT_2S			2000

/* NFC Controller I2C interface configuration */
//#define NFCC_I2C_INSTANCE     		1
#define NXPNCI_I2C_ADDR       		0x28U
//#define NXPNCI_I2C_BAUDRATE_KBPS	100

#define HIGH				1
#define LOW					0

extern void tml_Connect(void);
extern void tml_Send(uint8_t *pBuffer, uint16_t BufferLen, uint16_t *pBytesSent);
extern void tml_Receive(uint8_t *pBuffer, uint16_t BufferLen, uint16_t *pBytes, uint16_t timeout);

/*
 * globals.h
 *
 * Created: 18-9-2017 15:56:44
 *  Author: SanderBaten
 */ 


extern NxpNci_RfIntf_t RfInterface;
extern bool C_pass;
extern bool N_pass;
extern bool door_IsOpen;
extern bool door_IsClosed;
extern uint32_t CardID;
extern uint32_t LocalTime;
extern bool SendData;

#define Invalid 0x00
#define Npass 0x01
#define Cpass 0x02


#ifndef GLOBALS_H_
#define GLOBALS_H_


#endif /* GLOBALS_H_ */
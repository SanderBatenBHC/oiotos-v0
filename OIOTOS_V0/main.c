#define ITM_TER *(volatile int*)0xE0000E00 // Trace Enable Register
#define ITM_TCR *(volatile int*)0xE0000E80 // Trace Control Register
#define ITM_STIM_8(n) *(volatile uint8_t*)(0xE0000000 + ((n) * 4)) // 8 Bit N < 32
void SWO_print_char(int port, char c);
void SWO_print_string(int port, const char *s);

#include <atmel_start.h>
#include <stdio.h>
#include <string.h>
#include <tml.h>
#include <types.h>
#include <time.h>

//-----------------------------------------------------------------------
// Application Includes
//-----------------------------------------------------------------------
#include "NxpNci.h"
#ifdef CARDEMU_SUPPORT
#include <T4T_NDEF_EMU.h>
#endif

#ifdef P2P_SUPPORT
#include <P2P_NDEF.h>
#endif

#ifdef RW_SUPPORT
#include <RW_NDEF.h>
#endif

//-----------------------------------------------------------------------
// Typedefs
//-----------------------------------------------------------------------

typedef enum
{
    WELL_KNOWN_SIMPLE_TEXT,
    WELL_KNOWN_SIMPLE_URI,
    WELL_KNOWN_SMART_POSTER,
    MEDIA_VCARD,
    ABSOLUTE_URI,
    UNSUPPORTED_NDEF_MESSAGE = 0xFF
} NdefMessageType_e;


//-----------------------------------------------------------------------
// Function Prototypes
//-----------------------------------------------------------------------

#if defined P2P_SUPPORT || defined CARDEMU_SUPPORT
void NdefPush_Cb(unsigned char *pNdefRecord, unsigned short NdefRecordSize);
#endif

#if defined P2P_SUPPORT || defined RW_SUPPORT
void NdefPull_Cb(unsigned char *pNdefRecord, unsigned short NdefRecordSize);
#endif

NdefMessageType_e DetectNdefMessageType(unsigned char *pNdefRecord, unsigned short NdefRecordSize);

//-----------------------------------------------------------------------
// Constants
//-----------------------------------------------------------------------
#if defined P2P_SUPPORT || defined CARDEMU_SUPPORT
const char NDEF_RECORD[] =
{
    0xD1,   // MB/ME/CF/1/IL/TNF
    0x01,   // TYPE LENGTH
    0x07,   // PAYLOAD LENTGH
    'T',    // TYPE
    0x02,   // Status
    'e', 'n', // Language
    'T', 'e', 's', 't'
};
#endif

//-----------------------------------------------------------------------
// Global Variables
//-----------------------------------------------------------------------
#include <globals.h>
uint32_t LocalTime;
NxpNci_RfIntf_t RfInterface;
unsigned char DiscoveryTechnologies[] =
{
    MODE_POLL | TECH_PASSIVE_NFCA
    ,MODE_POLL | TECH_PASSIVE_NFCB
    ,MODE_POLL | TECH_PASSIVE_NFCF
#ifdef P2P_SUPPORT
    ,MODE_POLL | TECH_ACTIVE_NFCF
#endif
    ,MODE_LISTEN | TECH_PASSIVE_NFCA
#ifdef P2P_SUPPORT
    ,MODE_LISTEN | TECH_PASSIVE_NFCF
    ,MODE_LISTEN | TECH_ACTIVE_NFCA
    ,MODE_LISTEN | TECH_ACTIVE_NFCF
#endif
};
//-----------------------------------------------------------------------
// Macros
//-----------------------------------------------------------------------
#if defined P2P_SUPPORT || defined RW_SUPPORT
#define NDEF_PRINT_URI_CODE(x)					\
{												\
	switch(x)                                   \
	{                                           \
		case 1: /*PRINTF("http://www.")*/; break;       \
		case 2: /*PRINTF("https://www.")*/; break;      \
		case 3: /*PRINTF("http://")*/; break;           \
		case 4: /*PRINTF("https://")*/; break;          \
		case 5: /*PRINTF("tel:")*/; break;              \
		case 6: /*PRINTF("mailto:")*/; break;           \
		default: break;                             \
	}                                           \
}
#endif


void SWO_print_char(int port, char c)
{
    while ((ITM_STIM_8(port) & 1) == 0);
    ITM_STIM_8(port) = c;
}
void SWO_print_string(int port, const char *s)
{
    if ((ITM_TCR & 1) == 0) return;
    if ((ITM_TER & 1) == 0) return;
    while (*s) SWO_print_char(port, *s++);
    SWO_print_char(port, 0);
}

int main(void)
{
    /* Initializes MCU, drivers and middleware */
    //atmel_start_init();


    //RFID BLOCK START (To be moved to function!!)



    unsigned char mode = 0;


    // Configure board specific pin muxing
    //hardware_init();

    // Initialize UART terminal
    //dbg_uart_init();

    //OSA_Init();
    atmel_start_init();
	    gpio_set_pin_level(LEDYEL,TRUE);

delay_ms(3000);

// 	 gpio_set_pin_level(GSM_PON,true); //GSM OFF
// 	 gpio_set_pin_level(GSM_RST,false);
// 	 delay_ms(100);
// 	 gpio_set_pin_level(GSM_PON,false); //GSM ON
// 	 gpio_set_pin_level(GSM_RST,true);
// 	 delay_ms(500);

// 	GPS TEST
// 		for (int x = 0; x <5; x++)
// 		{
// 			delay_ms(1000);
// 		}
//
// 		struct io_descriptor *io, *io2;
// 	usart_sync_get_io_descriptor(&USART_0, &io);
// 	usart_sync_enable(&USART_0);
// 		io_write(io, (uint8_t *)"AT\r", 3);
// 		delay_ms(1000);
// 		io_write(io, (uint8_t*)"AT+GPSNMEA=0\r", 13);
// 		delay_ms(200);
// 		io_write(io, (uint8_t*)"AT+GPSPVT=1\r", 12);
// 		delay_ms(200);
// 		io_write(io, (uint8_t*)"AT+GPSSTART=0\r", 14);
// 		delay_ms(200);
//
// 		while(1)
// 		{
//
// 		}



    //RFID BLOCK END

    //Reset 2G MODULE
    delay_ms(2000);
    gpio_set_pin_level(GSM_PON,true); //GSM OFF
    gpio_set_pin_level(GSM_RST,false);
    delay_ms(2000);
    gpio_set_pin_level(GSM_PON,false); //GSM ON
    delay_ms(2000);
    gpio_set_pin_level(GSM_RST,true);
    delay_ms(2000);
    //Activate LED's for opto gates
    gpio_set_pin_level(LEDSW1,true);
    gpio_set_pin_level(LEDSW2,true);

    //Turn off bluetooth
    gpio_set_pin_level(BT_RST, LOW);



    delay_ms(500);

    //Add motor test code

    //Open lock
    //gpio_set_pin_level(MOTOR_IO1,HIGH); //nSleep
    //gpio_set_pin_level(MOTOR_IO2,HIGH);  //IN1
    //gpio_set_pin_level(MOTOR_IO3,HIGH); //EN1
    //gpio_set_pin_level(MOTOR_IO4,LOW); //IN2
    //gpio_set_pin_level(MOTOR_IO5,HIGH); //EN2

    // while(gpio_get_pin_level(ENDSW2) == true)
    //     {
    //         delay_ms(1);
    //     }

    gpio_set_pin_level(MOTOR_IO1,HIGH); //nSleep
    gpio_set_pin_level(MOTOR_IO2,HIGH);  //IN1
    gpio_set_pin_level(MOTOR_IO3,HIGH); //EN1
    gpio_set_pin_level(MOTOR_IO4,HIGH); //IN2
    gpio_set_pin_level(MOTOR_IO5,HIGH); //EN2
    delay_ms(10);
    door_IsOpen = true;
	door_IsClosed = false;

    if (gpio_get_pin_level(DOOR_SW) == LOW)
        {
            gpio_set_pin_level(MOTOR_IO1,HIGH); //nSleep
            gpio_set_pin_level(MOTOR_IO2,LOW);  //IN1
            gpio_set_pin_level(MOTOR_IO3,HIGH); //EN1
            gpio_set_pin_level(MOTOR_IO4,HIGH); //IN2
            gpio_set_pin_level(MOTOR_IO5,HIGH); //EN2

            while(gpio_get_pin_level(ENDSW1) == true)
                {
                    delay_ms(1);
                }

            gpio_set_pin_level(MOTOR_IO1,HIGH); //nSleep
            gpio_set_pin_level(MOTOR_IO2,HIGH);  //IN1
            gpio_set_pin_level(MOTOR_IO3,HIGH); //EN1
            gpio_set_pin_level(MOTOR_IO4,HIGH); //IN2
            gpio_set_pin_level(MOTOR_IO5,HIGH); //EN2
            delay_ms(100);
            door_IsClosed = true;
            door_IsOpen = false;
        }


// 			gpio_set_pin_level(MOTOR_IO1,HIGH); //nSleep
// 			gpio_set_pin_level(MOTOR_IO2,HIGH);  //IN1
// 			gpio_set_pin_level(MOTOR_IO3,HIGH); //EN1
// 			gpio_set_pin_level(MOTOR_IO4,LOW); //IN2
// 			gpio_set_pin_level(MOTOR_IO5,HIGH); //EN2
//
// 			while(gpio_get_pin_level(ENDSW2) == true)
// 			{
// 				delay_ms(1);
// 			}
//
//
//
// 	gpio_set_pin_level(MOTOR_IO1,HIGH); //nSleep
// 	gpio_set_pin_level(MOTOR_IO2,HIGH);  //IN1
// 	gpio_set_pin_level(MOTOR_IO3,HIGH); //EN1
// 	gpio_set_pin_level(MOTOR_IO4,HIGH); //IN2
// 	gpio_set_pin_level(MOTOR_IO5,HIGH); //EN2
// 	delay_ms(100);



    /*

    uint32_t LocalTime = 012345;
    char buffer[100];
    double LatDD,LongDD;
    LatDD = 51.869442;
    LongDD = 6.26388884;
    uint32_t iLatDD = LatDD * 1000000;
    uint32_t iLongDD = LongDD * 1000000;
    volatile int test = sprintf(buffer,"l%+09d,%+09d",iLatDD, iLongDD);
    SWO_print_string(0,buffer);


    */
    delay_ms(2000);
 

// struct io_descriptor *io;
// usart_sync_get_io_descriptor(&USART_0, &io);
// usart_sync_enable(&USART_0);
// io_write(io, (uint8_t *)"AT+CCLK?\r", 9);


char wmb[50];
    delay_ms(1000);


#ifdef CARDEMU_SUPPORT
/* Register NDEF message to be sent to remote reader */
T4T_NDEF_EMU_SetRecord((unsigned char *) NDEF_RECORD, sizeof(NDEF_RECORD), *NdefPush_Cb);
#endif

#ifdef P2P_SUPPORT
/* Register NDEF message to be sent to remote peer */
P2P_NDEF_SetRecord((unsigned char *) NDEF_RECORD, sizeof(NDEF_RECORD), *NdefPush_Cb);

/* Register callback for reception of NDEF message from remote peer */
P2P_NDEF_RegisterPullCallback(*NdefPull_Cb);
#endif

#ifdef RW_SUPPORT
/* Register callback for reception of NDEF message from remote cards */
RW_NDEF_RegisterPullCallback(*NdefPull_Cb);
#endif

/* Open connection to NXPNCI device */
if (NxpNci_Connect() == NXPNCI_ERROR)
{
	//PRINTF("\r\nError: cannot connect to NXPNCI device\r\n");
	while(1) {}
}

/* Set NXPNCI modes */
#ifdef CARDEMU_SUPPORT
mode |= NXPNCI_MODE_CARDEMU;
#endif
#ifdef P2P_SUPPORT
mode |= NXPNCI_MODE_P2P;
#endif
#ifdef RW_SUPPORT
mode |= NXPNCI_MODE_RW;
#endif

if (NxpNci_Configure(mode) == NXPNCI_ERROR)
{
	//PRINTF("\r\nError: cannot configure NXPNCI\r\n");
	while(1) {}
}

/* Start Discovery */
if (NxpNci_StartDiscovery(DiscoveryTechnologies,sizeof(DiscoveryTechnologies)) != NXPNCI_SUCCESS)
{
	//PRINTF("\r\nError: cannot start discovery\r\n");
	while(1) {}
}


gpio_set_pin_level(LEDYEL,FALSE);
sprintf(wmb,"Start..");
SWO_print_string(0,wmb);

//////////////////////////////////////////////////////////////////////////
//FETCH TIME FROM GPS...
//////////////////////////////////////////////////////////////////////////

bool GPSData = false;
uint8_t rx_buf[100] = "emtpy";
char fields[10][70];
int fieldPointer = 0;
int charPointer = 0;


    struct io_descriptor *io;
    usart_sync_get_io_descriptor(&USART_0, &io);
    usart_sync_enable(&USART_0);

    //io_write(io, (uint8_t *)"AT\r", 3);
   //delay_ms(1000);
  // io_write(io, (uint8_t *)"AT+KSIMDET=1,1\r", 15);
   //delay_ms(1000);
   //io_write(io, (uint8_t *)"AT+CREG=1\r", 10);
   //delay_ms(1000);
   //io_write(io, (uint8_t *)"AT&W\r", 5);
   delay_ms(1000);
   
    io_write(io, (uint8_t*)"AT+GPSNMEA=0\r", 13);
    delay_ms(200);
    io_write(io, (uint8_t*)"AT+GPSPVT=1,1,1\r", 16);
    delay_ms(200);
    io_write(io, (uint8_t*)"AT+GPSSTART=0\r", 14);
    delay_ms(5000);

    //                             while(!usart_sync_is_rx_not_empty(&USART_0))
    //                                 ;
  
    rx_buf[0]= "\0";
    io_read(io,&rx_buf,30);
    

    sprintf(wmb,"Data In: %s",rx_buf);
    SWO_print_string(0,wmb);

    while (GPSData == false)
        {
		gpio_toggle_pin_level(LEDGRN);

            //while(!usart_sync_is_rx_not_empty(&USART_0))
            //    ;
            
            rx_buf[0]= "\0";
            io_read(io,&rx_buf,70);
            //rx_buf[5] = '\0';
            sprintf(wmb,"Data: %s",rx_buf);
            SWO_print_string(0,wmb);


            for (int p = 0; p < 70; p++)
                {
                    if (rx_buf[p] == ',')
                        {
                            fields[fieldPointer][charPointer] = '\0';
                            fieldPointer++;
                            charPointer = 0;

                        }
                    else
                        {
                            fields[fieldPointer][charPointer] = rx_buf[p];
                            charPointer++;
                        }

                }
            fieldPointer = 0;
            charPointer = 0;

            sprintf(wmb,"Data In: %s",rx_buf);
            SWO_print_string(0,wmb);
            sprintf(wmb,"Msg type: %s",fields[0]);
            SWO_print_string(0,wmb);
            sprintf(wmb,"Time: %s",fields[1]);
            SWO_print_string(0,wmb);
            sprintf(wmb,"Date: %s",fields[2]);
            SWO_print_string(0,wmb);
            sprintf(wmb,"Fix Status: %s",fields[3]);
            SWO_print_string(0,wmb);
            sprintf(wmb,"Lat: %s",fields[4]);
            SWO_print_string(0,wmb);
            sprintf(wmb,"Long: %s",fields[5]);
            SWO_print_string(0,wmb);
            //sprintf(wmb,"Data In6: %s",fields[6]);
            //SWO_print_string(0,wmb);
            //sprintf(wmb,"Data In7: %s",fields[7]);
            //SWO_print_string(0,wmb);
            if ((strncmp(fields[3],"2D",2) == 0) || (strncmp(fields[3],"3D",2) == 0))
                {
                    sprintf(wmb,"Valid fix: %s",fields[3]);
                    SWO_print_string(0,wmb);
                    GPSData = true;
                }

            //GPSData = true;
            //#warning Test Data!!
        }
    io_write(io, (uint8_t*)"AT+GPSSTOP\r", 11);


    //////////////////////////////////////////////////////////////////////////
    //END
    //////////////////////////////////////////////////////////////////////////


    struct tm ts;
    char buf[80];
    time_t t_of_day;

int days, months, years, hours, minutes, seconds;

days = ( ((fields[2][0]- '0')*10) + ((fields[2][1]- '0')*1) );
months = ( ((fields[2][3]- '0')*10) + ((fields[2][4]- '0')*1) );
years = ( ((fields[2][6]- '0')*1000) + ((fields[2][7]- '0')*100) + ((fields[2][8]- '0')*10)+ ((fields[2][9]- '0')*1) );

hours = ( ((fields[1][0]- '0')*10) + ((fields[1][1]- '0')*1) );
minutes = ( ((fields[1][3]- '0')*10) + ((fields[1][4]- '0')*1) );
seconds = ( ((fields[1][6]- '0')*10) + ((fields[1][7]- '0')*1) );


    //ts.tm_year = 2017-1900;
    ts.tm_year = years - 1900;
	//ts.tm_mon = 8; //Current - 1!!
	ts.tm_mon = months - 1;
    //ts.tm_mday = 24;
	ts.tm_mday = days;
    //ts.tm_hour = 15;
	ts.tm_hour = hours;
    //ts.tm_min = 8;
	ts.tm_min = minutes;
    //ts.tm_sec = 52;
	ts.tm_sec = seconds;
    ts.tm_isdst = -1;
    t_of_day = mktime(&ts);

    char buffer[100];
    sprintf(buffer,"Time in UNIX: %d", (long) t_of_day);
    SWO_print_string(0,buffer);
	LocalTime = (long) t_of_day;


gpio_set_pin_level(LEDGRN,TRUE);
delay_ms(2000);
gpio_set_pin_level(LEDGRN,FALSE);


    delay_ms(100);
    SWO_print_string(0,"System booting RTOS...");
    FREERTOS_V823_0_example();
    //RTOS Running....
    while (1)
        {
        }
}



#if defined P2P_SUPPORT || defined CARDEMU_SUPPORT
void NdefPush_Cb(unsigned char *pNdefRecord, unsigned short NdefRecordSize)
{
    //PRINTF("\r\n--- NDEF Record sent\r\n");
}
#endif

#if defined P2P_SUPPORT || defined RW_SUPPORT
/* The callback only print out the received message */
void NdefPull_Cb(unsigned char *pNdefRecord, unsigned short NdefRecordSize)
{
    char *pString;

    //PRINTF("\r\n--- NDEF Record received:\r\n");

    /* vCard NDEF message? */
    if(DetectNdefMessageType(pNdefRecord, NdefRecordSize) == MEDIA_VCARD)
        {
            //PRINTF("\r\n   vCard found:\r\n");

            /* Short record or normal record */
            if (pNdefRecord[0] & NDEF_RECORD_SR_MASK)
                {
                    pString = strstr((char*)&pNdefRecord[3], "BEGIN");
                    //PRINTF("\r\n%s", pString);
                }
            else
                {
                    pString = strstr((char*)&pNdefRecord[6], "BEGIN");
                    //PRINTF("\r\n%s", pString);
                }
        }
    /* Short, not fragmented and well-known type record */
    else if (pNdefRecord[0] == 0xD1)
        {
            switch (pNdefRecord[3])
                {
                case 'T':
                    pNdefRecord[7 + pNdefRecord[2]] = '\0';
                    //PRINTF("\r\n   Text record (language = %c%c): %s\r\n", pNdefRecord[5], pNdefRecord[6], &pNdefRecord[7]);
                    break;

                case 'U':
                    //PRINTF("\r\n   URI record: ");
                    NDEF_PRINT_URI_CODE(pNdefRecord[4])
                    pNdefRecord[4 + pNdefRecord[2]] = '\0';
                    //PRINTF("%s\r\n", &pNdefRecord[5]);
                    break;

                default:
                    //PRINTF("\r\n   Unsupported NDEF record, only 'Text', 'URI' or 'Media vCard' types are supported\r\n");
                    break;
                }
        }
    else
        {
            //PRINTF("\r\nUnsupported NDEF record, cannot parse\r\n");
        }

    pNdefRecord[0] = 0x0;

    //PRINTF("\r\n");
}
#endif

/*------------------------------------------------------------
 *
 * Function Name : DetectNdefMessageType
 *
 * Description: Parses the NDEF message to detect what kind of
 * 				records it contains
 *
 *-----------------------------------------------------------*/

NdefMessageType_e DetectNdefMessageType(unsigned char *pNdefRecord, unsigned short NdefRecordSize)
{
    uint8_t typeField;
    NdefMessageType_e NdefMessageType = UNSUPPORTED_NDEF_MESSAGE;

    /* Preset type to "unsupported" in case of no match */
    NdefMessageType = UNSUPPORTED_NDEF_MESSAGE;

    /* Short record or normal record */
    if (pNdefRecord[0] & NDEF_RECORD_SR_MASK) typeField = 3;
    else typeField = 6;

    /* Well known Record Type? */
    if((pNdefRecord[0] & NDEF_RECORD_TNF_MASK) == NDEF_WELL_KNOWN)
        {
            if(pNdefRecord[1] == 0x1)
                {
                    switch(pNdefRecord[typeField])
                        {
                        case 'T':
                            NdefMessageType = WELL_KNOWN_SIMPLE_TEXT;
                            break;
                        case 'U':
                            NdefMessageType = WELL_KNOWN_SIMPLE_URI;
                            break;
                        }
                }
            else if(pNdefRecord[1] == 0x2)
                {
                    if(memcmp(&pNdefRecord[typeField], "Sp", pNdefRecord[1]) == 0x0)
                        {
                            NdefMessageType = WELL_KNOWN_SMART_POSTER;
                        }
                }
        }
    /* Media Record Type? */
    else if((pNdefRecord[0] & NDEF_RECORD_TNF_MASK) == NDEF_MEDIA)
        {
            if((memcmp(&pNdefRecord[typeField], "text/x-vCard", pNdefRecord[1]) == 0x0) || (memcmp(&pNdefRecord[typeField], "text/vcard", pNdefRecord[1]) == 0x0))
                {
                    NdefMessageType = MEDIA_VCARD;
                }
        }
    /* Absolute URI Record Type? */
    else if((pNdefRecord[0] & NDEF_RECORD_TNF_MASK) == NDEF_ABSOLUTE_URI)
        {
            NdefMessageType = ABSOLUTE_URI;
        }

    return NdefMessageType;
}

////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////

